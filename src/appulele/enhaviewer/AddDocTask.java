package appulele.enhaviewer;

import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.ConsoleMessage;
import android.webkit.ConsoleMessage.MessageLevel;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Toast;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;
import appulele.enhaviewer.strings.C;

@SuppressLint("SetJavaScriptEnabled")
public class AddDocTask extends AsyncTask<Void, Void, Void> {
	/**
	 * a 태그의 class로 구분한다 wiki 를 포함하면 : Move external : 외부이미지 externalLink : 외부링크
	 * a 태그의 href로 구분한다 #fn 을 포함하면 : 주석. 해당 a태그의 title을 출력해준다.
	 */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private final boolean MORBILE_PAGE = false;
	private final boolean NORMAL_PAGE = true;
	final String MODE_MIRROR = "MIRROR";
	final String MODE_WIKIPIDIA = "WIKIPIDIA";
	private static String mode = "MIRROR";
	private Context mContext;
	private String convertHTML;

	private MainActivity mActivity;
	private ArrayList<LinkData> mLinkDataList;
	private ArrayList<LinkData> mFootDataList;
	private ArrayList<String> movieList;
	private ArrayList<String> iframeList;
	private String wikiURL;
	private EnhaDocument mSelectedDocument;
	private boolean hasParent = false;
	private int fontSize;
	boolean curPageMode = NORMAL_PAGE;
	SharedPreferences sharedPref;
	mySharedPrefrence myPref;
	// AddDocTask이후 생성되는 데이터
	private WebView wv;
	private String tabName;

	boolean makeMovieBtn = false;
	private boolean newFootDlg = false;// 주석 새창모드
	private boolean blackMode = true; // 전체 색상 모드
	private boolean directMove = true;// 링크 바로이동/현문서 유지

	boolean isLoading = false;

	/**
	 * 최상위 DOCUMENT 문서를 ASYNKTASK로 추가한다.
	 * 
	 * @param url
	 * @param name
	 * @param context
	 */
	public AddDocTask(String url, String name, Context context) {
		Log.d(TAG, "tree:  AddDocText init, no Parent");
		mContext = context;
		mActivity = (MainActivity) context;
		this.tabName = name;
		this.wikiURL = url.replace('+', ' ');
		sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
		myPref = new mySharedPrefrence(mContext, sharedPref);
		fontSize = myPref.getFontSize();
	
		newFootDlg = myPref.isNewFootDlg();
		blackMode = myPref.isBlackMode();
	}

	/**
	 * 자식 DOCUMENT 문서를 ASYNKTASK로 추가한다.
	 * 
	 * @param selectedDoc
	 * @param url
	 * @param name
	 * @param context
	 */
	public AddDocTask(EnhaDocument selectedDoc, String url, String name, Context context) {
		Log.d(TAG, "tree:  AddDocText init, has Parent");
		mContext = context;
		mActivity = (MainActivity) context;
		this.tabName = name;
		this.wikiURL = url.replace('+', ' ');
		this.mSelectedDocument = selectedDoc;
		this.hasParent = true;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
		myPref = new mySharedPrefrence(mContext, sharedPref);
		fontSize = myPref.getFontSize();
		//Toast.makeText(mActivity, "2size:" + fontSize, Toast.LENGTH_SHORT).show();
		newFootDlg = myPref.isNewFootDlg();
		blackMode = myPref.isBlackMode();
		directMove = myPref.isDirectMove();
	}

	@Override
	protected Void doInBackground(Void... params) {
		try {
			convertHTML = convertHTML(wikiURL);
		} catch (Exception e) {
			Log.d(TAG, "convert Error");
		}
		return null;
	}

	/**
	 * 모든 ASYNK 끝난 후에 동작붑분 MAIN ACTIVITY에 생성한 DOC을 추가해준다.
	 */

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		// type 0,1을 구분해서 동작
		wv = createWebView(mContext, convertHTML);
		EnhaDocument createdDocument = null;
		if (hasParent) {
			createdDocument = new EnhaDocument(mSelectedDocument, tabName, wv);
			try {
				wv.addJavascriptInterface(WebViewJavaScriptObject.getObject(mContext, createdDocument), "AppInterface");
			} catch (Exception e) {
				Toast.makeText(mContext, "error", Toast.LENGTH_LONG).show();
			}

			createdDocument.setTab(createTab(createdDocument, mContext));
			createdDocument.setUrl(wikiURL);
			mActivity.addDoc(createdDocument, mSelectedDocument, wikiURL);
			// mActivity.addDoc(createdDocument, wikiURL);
		} else {
			createdDocument = new EnhaDocument(tabName, wv);
			wv.addJavascriptInterface(WebViewJavaScriptObject.getObject(mContext, createdDocument), "AppInterface");
			createdDocument.setTab(createTab(createdDocument, mContext));
			createdDocument.setUrl(wikiURL);
			mActivity.addDoc(createdDocument, wikiURL);
		}

	}

	/**
	 * 새 탭 추가
	 * 
	 * @param doc
	 * @param mContext
	 * @return
	 */
	public static LinearLayout createTab(EnhaDocument doc, Context mContext) {
		final MainActivity mActivity = (MainActivity) mContext;
		LayoutInflater inflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		LinearLayout tab = null;
		tab = (LinearLayout) inflater.inflate(R.layout.tab, tab);
		Button btn = (Button) tab.findViewById(R.id.buttonTabText);
		btn.setOnClickListener(mActivity);

		String tabName = doc.getCurTabName();

		btn.setText(tabName);
		btn.setSingleLine();
		btn.setTag(doc);
		doc.setBtnTab(btn);
		ImageButton btnClose = (ImageButton) tab.findViewById(R.id.ibTabClose);
		btnClose.setTag(doc);
		btnClose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View btnClose) {
				EnhaDocument doc = (EnhaDocument) btnClose.getTag();
				mActivity.deleteDoc(doc);
			}
		});

		btnClose = mActivity.CreateCloseBtn(doc);
		tab.setTag(doc);

		return tab;
	}

	/**
	 * wikiURL을 이용해 URL의 HTML을 변환한다.
	 * 
	 * @param wikiURL
	 * @return
	 */
	private String convertHTML(String wikiURL) {
		String originalHTML;
		Document doc = null;
		mLinkDataList = new ArrayList<LinkData>();
		mFootDataList = new ArrayList<LinkData>();
		movieList = new ArrayList<String>();
		iframeList = new ArrayList<String>();
	 
		// 해당 url의 HTML 받아옴
		try {
			doc = Jsoup.connect(wikiURL).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 연결이 안되는 등 문제가 생기면 알림(타임아웃 인터넷 꺼놓음 등 )
		originalHTML ="데이터를 불러올 수 없습니다. 다시 불러와 주세요.";
		 for (int i = 0; i < 4; i++) {
			 try {
					originalHTML = doc.html();
					 
				//	Toast.makeText(mActivity, "converting:" +"OK", Toast.LENGTH_SHORT).show();
					i=4;
				} catch (Exception e) {
				 
				 
					// return "<h1> 현재 응답이 없습니다, 다시 시도 해 주세요. </h1>"; 
				}
		}
			
	 
		mode = MainActivity.MODE;
		// originalHTML = originalHTML.replaceAll("src=\"//","src=\"http://");
		// 모드에 따라 색상 변경
		if (blackMode) {
			originalHTML = originalHTML.replace(C.whiteModeA, C.blackModeA);
			originalHTML = originalHTML.replace(C.whiteModeB, C.blackModeB);
		}

		// 위키피디아에서 한번만 바꿔주면 되는 부분
		 if (wikiURL.contains("wikipedia")) {
		originalHTML = originalHTML.replaceAll("//upload", "http://upload");
		originalHTML = originalHTML.replaceAll("/w/index.php?", URLs.URLPidia + "/w/index.php?");

		originalHTML = originalHTML.replace("<div id=\"mw-search-top-table\">", "<div id=\"mw-search-top-table\"  style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"search-types\">", "<div class=\"search-types\"     style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"header\">", "<div class=\"header\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div id=\"mw-mf-viewport\">", "<div id=\"mw-mf-viewport\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"header\">", "<div class=\"header\"  style=\"visibility: hidden; display:none;  >");
		originalHTML = originalHTML.replace("<div class=\"pre-content\">", "<div class=\"pre-content\"  style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div id=\"footer\">", "<div id=\"footer\"  style=\"visibility: hidden; display:none; >");
		originalHTML = originalHTML.replace("<a class=\"mw-ui-button mw-ui-progressive button languageSelector\"",
					"<a class=\"mw-ui-button mw-ui-progressive button languageSelector\"  style=\"visibility: hidden; display:none; >");
		originalHTML = originalHTML.replace("<a id=\"mw-mf-last-modified\"", "<a id=\"mw-mf-last-modified\"  style=\"visibility: hidden; display:none; >");
		 }
		// } else {
		// 엔하위키에서 한번만 바꿔주면 되는 부분
		// HTML내부의 필요없는 부분 지우기
		// 일반화면 and 모바일 화면
		// if (curPageMode == MORBILE_PAGE) {
		originalHTML = originalHTML.replace("<div class=\"toolbar\">", "<div class=\"toolbar\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"toolbox\">", "<div class=\"toolbox\" style=\"visibility: hidden; display:none;>");

		// } else if (curPageMode == NORMAL_PAGE) {
		originalHTML = originalHTML.replace("<div class=\"boilerplate\">", "<div class=\"boilerplate\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"disclaimer\">", "<div class=\"disclaimer\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"search-box\">", "<div class=\"search-box\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"search-form\">", "<div class=\"search-form\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div id=\"_toolbox\">", "<div id=\"_toolbox\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div id=\"_toolbox\">", "<div id=\"_toolbox\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<div class=\"wrapper\">", "<div class=\"wrapper\" style=\"visibility: hidden; display:none;>");
		originalHTML = originalHTML.replace("<p class=\"character\">", "<p class=\"character\"  style=\"visibility: hidden; display:none;>");

		// }
		// }
		// 위키피디아 주석 처리 위한 부분
		if (newFootDlg) {
			Elements elementLinkFoot = doc.select("li[id*=cite]");
			for (Element link : elementLinkFoot) {
				LinkData linkData = new LinkData(link);
				mFootDataList.add(linkData);
			}

		}
		// 일반적인 정보
		Elements elementLinks = doc.select("a[href]");
		// HTML내부의 모든 링크를 mLinkDataList에 추가
		for (Element link : elementLinks) {
			LinkData linkData = new LinkData(link);
			mLinkDataList.add(linkData);
		}

		// 동영상 주소 모음

		// Elements elementMoviesIframe = doc.select("iframe[src],embed[src]");
		Elements elementMoviesIframe = doc.select("iframe[src]");
		// HTML내부의 모든 링크를 mLinkDataList에 추가
		for (Element link : elementMoviesIframe) {
			iframeList.add(link.attr("src"));
			Log.d(TAG, "iframe Movies: " + link.attr("src"));
		}

		// iframe 일단 비사용 에러안나는거같음
		Elements elementMoviesEmbed = doc.select("embed[src]");
		for (Element link : elementMoviesEmbed) {
			// LinkData linkData = new LinkData(link);
			movieList.add(link.attr("src"));
			Log.d(TAG, "embed Movies: " + link.attr("src"));
		}

		// 링크 담아놓을 ArrayList선언
		ArrayList<LinkData> wikiLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> externalLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> externalLinkLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> footLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> findNextList = new ArrayList<LinkData>();
		ArrayList<LinkData> othersList = new ArrayList<LinkData>();

		// HTML 링크 클릭시 함수 동작
		for (int i = 0; i < mLinkDataList.size(); i++) {
			LinkData curLinkData = mLinkDataList.get(i);
			String linkClass = curLinkData.getLinkClass();
			String linkTitle = curLinkData.getTitle();
			String linkUrl = curLinkData.getUrl();
			Log.d(TAG, "nextList : class =" + linkClass + " url :" + linkUrl + "  title :" + linkTitle);

			if (linkClass.equals("wiki")) {
				wikiLinkDataList.add(curLinkData);
			} else if (linkClass.equals("external")) {
				externalLinkDataList.add(curLinkData);
			} else if (linkClass.equals("externalLink")) {
				externalLinkLinkDataList.add(curLinkData);
			} else if (linkUrl.contains("#fn") || linkUrl.contains("#i") || linkUrl.contains("#v") || linkUrl.contains("#cite_note")) {
				if (newFootDlg) {
					footLinkDataList.add(curLinkData);
					Log.d(TAG, "nextList : test:" + linkTitle);
					findNextList.add(curLinkData);
				}
			} else {
				othersList.add(curLinkData);
			}
		}

		// Log
		if (D) {
			String listNameArray[] = new String[] { "wiki", "external", "externalLink", "foot", "others" };
			ArrayList<ArrayList<LinkData>> listArray = new ArrayList<ArrayList<LinkData>>();
			listArray.add(wikiLinkDataList);
			listArray.add(externalLinkDataList);
			listArray.add(externalLinkLinkDataList);
			listArray.add(footLinkDataList);
			listArray.add(othersList);

			for (int i = 0; i < listArray.size(); i++) {
				ArrayList<LinkData> curLinkDataList = listArray.get(i);
				String type = listNameArray[i];
				Log.d(TAG, "-- " + type + " Start --");
				for (int j = 0; j < curLinkDataList.size(); j++) {
					LinkData curLinkData = curLinkDataList.get(j);
					Log.d(TAG, type + " [" + j + "] : " + curLinkData.getTitle() + ", " + curLinkData.getUrl());
				}
				Log.d(TAG, "-- " + type + " End --");
			}
		}

		
		originalHTML = originalHTML.replaceAll( "//rv.wkcdn.net/",  "http://rv.wkcdn.net/");
		/**
		 * href="// ~ "; 인 모든 링크주소 href="http:// ~"; 로 바꾸기 href="//~" ->
		 * href="http://~"
		 */
		String wikiRegex = "href=\"//([^\"]+)\"";
		originalHTML = originalHTML.replaceAll(wikiRegex, "href=\"" + "javascript:AppInterface.others(" + "\'$1\'" + ")\"");

		/**
		 * href="/wiki/([^\"]+)\""; 인 모든 링크주소 한 번에 바꾸기 ex)
		 * href="/wiki/starcraft" ->
		 * href="javascript:AppInterface.wiki("starcraft")"
		 */
		wikiRegex = "href=\"/wiki/([^\"]+)\"";
		originalHTML = originalHTML.replaceAll(wikiRegex, "href=\"" + "javascript:AppInterface.wiki(" + "\'$1\'" + ")\"");

		/**
		 * href="/search?q=(keyword)&p=(page_num) href="/search?q=Korea&p=2 ->
		 * href="javascript:AppInterface.page("/search?q=Korea&p=2")
		 */
		String pageRegex = "href=\"/search([^\"]+)\"";
		// originalHTML = originalHTML.replaceAll(pageRegex, "href=\"" +
		// "javascript:AppInterface.page(" + "\'$1\'" + ")\"");
		originalHTML = originalHTML.replaceAll(pageRegex, "href=\"" + "javascript:AppInterface.page(" + "\'$1\'" + ")\"");

		/**
		 * href=
		 * "http://ko.m.wikipedia.org/w/index.php?title=%ED%8A%B9%EC%88%98:%EA%B2%80%EC%83%89&limit=(num)&offset=(num)&profile=default&search=Korea"
		 * -> href="javascript:AppInterface.page("http://ko.m.wikipedia.org/w/
		 * index
		 * .php?title=%ED%8A%B9%EC%88%98:%EA%B2%80%EC%83%89&limit=(num)&offset
		 * =(num)&profile=default&search=Korea")
		 */

		/**
		 * 외부 이미지 링크 함수로 바꾸기
		 * */
		for (int i = 0; i < externalLinkDataList.size(); i++) {
			// 외부 주소의 - 문자를 html형으로 변환
			String url = externalLinkDataList.get(i).getUrl();
			String urlConvert = externalLinkDataList.get(i).getUrl().replace("-", "&#45;");// .replace("(",
			// "&#40;").replace(")"," &#41;");
			originalHTML = originalHTML.replaceAll("href=\"" + url, "href=\"" + "javascript:AppInterface.external(\'" + urlConvert + "\')");
			Log.d("externalLink", "externalLink URL CONVERT:" + urlConvert);
		}

		/**
		 * 주석 함수로 바꾸기
		 * */
		for (int i = footLinkDataList.size() - 1; i >= 0; i--) {
			String url = footLinkDataList.get(i).getUrl();
			// String형의 특수문자를 HTML의 특수 문자로 변환한다.
			String text = "";

			// 주석 처리하기
			if (mode.equals(MODE_MIRROR)) {
				text = footLinkDataList.get(i).getFoot().replace("\"", "&quot;").replace("\'", "&quot;");
			} else if (mode.equals(MODE_WIKIPIDIA)) {
				for (int j = 0; j < mFootDataList.size(); j++) {
					String footText = "#" + mFootDataList.get(j).getId();
					if (footText.equals(url)) {
						text = mFootDataList.get(j).getText();
						break;
					}
				}
			}
			try {

				originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.foot(\'" + text + "\')");

			} catch (Exception e) {
				Log.d(TAG, "error");
			}
		}

		/**
		 * 외부 링크 웹뷰로 띄우기
		 * */

		for (int i = 0; i < externalLinkLinkDataList.size(); i++) {
			String url = externalLinkLinkDataList.get(i).getUrl();
			String text = externalLinkLinkDataList.get(i).getTitle();
			originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.externalLink(\'" + url + "\')");
		}

		// //동영상 링크 만들기
		makeMovieBtn = myPref.isCreateMovieBtn();
		if (makeMovieBtn) {
			String[] cutHtml = originalHTML.split("<embed");
			originalHTML = "";
			for (int i = 0; i < cutHtml.length; i++) {
				// String tempURL = URLChecker.urlChecker(iframeList.get(i -
				// 1));
				// Log.d(TAG, "tempURL:"+tempURL);
				if (i != 0)
					cutHtml[i] = "<a href=\"" + movieList.get(i - 1) + "\" >동영상 보기\n  (동영상이 보이지 않으면 FLASH 지원이 되는 \"FLASH FOX\" 애플리케이션을 설치 해 주세요.)</a> <embed " + cutHtml[i];
				originalHTML += cutHtml[i];
				Log.d(TAG, "cutHTML :" + i + "  " + cutHtml[i]);
			}
		}

		// //다음 페이지 누르면 이동
		// for (int i = 0; i < externalLinkLinkDataList.size(); i++) {
		// String url = externalLinkLinkDataList.get(i).getUrl();
		// String text = externalLinkLinkDataList.get(i).getTitle();
		// originalHTML = originalHTML.replaceAll(url,
		// "javascript:AppInterface.externalLink(\'" + url + "\')");
		// }

//		// Table없애기
//		originalHTML = originalHTML.replace(URLs.table1, URLs.tt1);
//		originalHTML = originalHTML.replace(URLs.table2, URLs.tt2);
		return originalHTML;

	}

	@SuppressWarnings("deprecation")
	private WebView createWebView(final Context mContext, final String convertHTML) {
		wv = new WebView(mContext);
		// wv.setBackgroundColor(Color.BLUE);
		wv.loadData(convertHTML, "text/html; charset=utf-8", "utf-8");
		// wv.loadDataWithBaseURL(null, convertHTML, "text/html", "UTF-8",
		// null);
		// 생성된 webView를 새로이 EnhaDoc를 생성해서 부모로 Link를 정해준다.
		Log.d(TAG, "hasParent webView");
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ScrollView sv = new ScrollView(mContext);
		sv.setLayoutParams(params);
		// 웹뷰로 js alert보기위한 설정
		// fix3 webView 에 태그 달기

		wv.setHorizontalScrollBarEnabled(false); // 세로 scroll 제거
		wv.setVerticalScrollBarEnabled(false); // 가로 scroll 제거/ //

		WebSettings wvSettings = wv.getSettings();

		// if (!blackMode){// 웹뷰 색상 모드
		// wv.setBackgroundColor(Color.rgb(60, 60, 60));
		//
		// }
		wvSettings.setLoadWithOverviewMode(true);
		wvSettings.setDefaultFontSize(fontSize);
		wvSettings.setDefaultFixedFontSize(fontSize);
		wvSettings.setMinimumFontSize(fontSize - 2);
		// wvSettings.setDefaultZoom(ZoomDensity.FAR);
		// //
		// wv.getSettings().setPluginState(PluginState.ON);//flash 웹뷰로 보여주기

		wv.getSettings().setJavaScriptEnabled(true);
		// 큰 이미지 사이즈 맞추기

		// Table없어진 HTML을 공간에 맞춰서 보여주기
//		wv.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
//		wv.setFocusableInTouchMode(false);
//		wv.setFocusable(false);
		// if (mode.equals(MODE_WIKIPIDIA)) {
		//
		// wv.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		// } else if (mode.equals(MODE_MIRROR)) {
		// wv.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		// }
//		wv.setWebChromeClient(new WebChromeClient() {
//		 
//
//		});

		wv.setOnLongClickListener(mActivity);
		// 웹뷰로 새창 열기
		wv.setOnTouchListener(mActivity);

		wv.setWebViewClient(new WebViewClient() {

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {

				isLoading = true;

			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// return false;
				try {
					URL urlObj = new URL(url);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(url));
					mActivity.startActivity(intent);
					return true;

				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		});

		return wv;
	}
}
