package appulele.enhaviewer;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.EditText;
import android.widget.Toast;

/**
 * 검색을 위해 이동, 검색 버튼 클릭시 동작 함수 구현 클래스
 * 
 * @author sanghyeon
 */
public class ClickMoveButton {
	private final boolean D = false;
	private final String MODE_MIRROR = "MIRROR";
	private final String MODE_WIKIPIDIA = "WIKIPIDIA";
	Context mContext;
	MainActivity mainActivity;
	EditText etMain;
	final static String TAG = "ClickMoveButton";
	String urlMode;
	String urlModeFind;
	String urlModeFind2 = "";

	public ClickMoveButton(Context context) {
		mContext = context;
		mainActivity = (MainActivity) mContext;
		etMain = mainActivity.getEtMain();
		if (MainActivity.MODE.equals(MODE_MIRROR)) {
			urlMode = URLs.URLEnhaWiki;
			urlModeFind = URLs.URLEnha + "/search/?q=";
		} else if (MainActivity.MODE.equals(MODE_WIKIPIDIA)) {
			urlMode = URLs.URLPidiaWiki;
			urlModeFind = URLs.URLPidiaSearchA;
			urlModeFind2 = URLs.URLPidiaSearchB;
		}
	}

	/**
	 * 검색 함수 - 인터넷 상태 체크후에 ENHADOCUMENT 문서 추가
	 * 
	 * @param selectedDoc
	 * @param lastDoc
	 */
	public void find(EnhaDocument selectedDoc, EnhaDocument lastDoc) {
		boolean networkConnect = NetworkCheck.networkCheck(mContext);
		if (!networkConnect) {
			Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			return;
		}

		if (!etMain.getText().toString().equals("") && !etMain.getText().toString().equals(null)) {
			checkWiki();
			String keyword = etMain.getText().toString();
			String encodedKeyword = "";
			try {
				encodedKeyword = URLEncoder.encode(etMain.getText().toString(), "UTF-8");
				encodedKeyword.replace('+', ' ');
				Log.d("encodeKeyword", "keyword:" + encodedKeyword);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}

			AddDocTask addDocTask = new AddDocTask(lastDoc, urlModeFind + encodedKeyword + urlModeFind2, etMain.getText().toString(), mContext);
			addDocTask.execute();
			etMain.setText("");
		}
	}

	/**
	 * 이동 함수 - 인터넷 상태 체크후에 ENHADOCUMENT 문서 추가
	 * 
	 * @param selectedDoc
	 * @param lastDoc
	 */
	public void move(EnhaDocument selectedDoc, EnhaDocument lastDoc) {
		boolean networkConnect = NetworkCheck.networkCheck(mContext);
		if (!networkConnect) {
			Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			return;
		}
		if (!etMain.getText().toString().equals("") && !etMain.getText().toString().equals(null)) {
			String keyword = etMain.getText().toString();
			String encodedKeyword = "";
			checkWiki();
			try {
				encodedKeyword = URLEncoder.encode(etMain.getText().toString(), "UTF-8");
				encodedKeyword.replace('+', ' ');
				Log.d("encodeKeyword", "keyword:" + encodedKeyword);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			Log.d(TAG, "Move URL: " + "http://m.enha.kr/wiki/" + encodedKeyword + "  keyword:" + keyword);
			AddDocTask addDocTask = new AddDocTask(lastDoc, urlMode + encodedKeyword, etMain.getText().toString(), mContext);
			addDocTask.execute();
			etMain.setText("");
		}
	}

	/**
	 * 검색 함수
	 * 
	 * @param selectedDoc
	 */
	public void find(EnhaDocument selectedDoc) {
		Log.d(TAG, "tree:  in FindClick");
		boolean networkConnect = NetworkCheck.networkCheck(mContext);
		if (!networkConnect) {
			Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			return;
		}
		if (!etMain.getText().toString().equals("") && !etMain.getText().toString().equals(null)) {
			String keyword = etMain.getText().toString();
			String encodedKeyword = "";
			checkWiki();
			try {
				encodedKeyword = URLEncoder.encode(etMain.getText().toString(), "UTF-8");
				encodedKeyword.replace('+', ' ');
				Log.d("encodeKeyword", "keyword:" + encodedKeyword);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			AddDocTask addDocTask = new AddDocTask(urlModeFind + encodedKeyword + urlModeFind2, etMain.getText().toString(), mContext);
			addDocTask.execute();
			etMain.setText("");
		}
	}

	/**
	 * 이동 함수
	 * 
	 * @param selectedDoc
	 */
	public void move(EnhaDocument selectedDoc) {
		Log.d(TAG, "tree:  MoveClick");
		boolean networkConnect = NetworkCheck.networkCheck(mContext);
		if (!networkConnect) {
			Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			return;
		}
		if (!etMain.getText().toString().equals("") && !etMain.getText().toString().equals(null)) {
			String keyword = etMain.getText().toString();
			String encodedKeyword = "";
			checkWiki();
			try {
				encodedKeyword = URLEncoder.encode(etMain.getText().toString(), "UTF-8");
				encodedKeyword.replace('+', ' ');
				Log.d("encodeKeyword", "keyword:" + encodedKeyword);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			Log.d(TAG, "Move URL: " + "http://m.enha.kr/wiki/" + encodedKeyword + "  keyword:" + keyword);
			AddDocTask addDocTask = new AddDocTask(urlMode + encodedKeyword, etMain.getText().toString(), mContext);
			addDocTask.execute();
			etMain.setText("");
		}
	}

	/**
	 * 이동함수 - 저장된 이름만 가지고도 불러 옴
	 * 
	 * @param savedName
	 */
	public void move(String savedName) {

		boolean networkConnect = NetworkCheck.networkCheck(mContext);
		if (!networkConnect) {
			Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			return;
		}

		String keyword = savedName;
		String encodedKeyword = "";
		checkWiki();
		try {
			encodedKeyword = URLEncoder.encode(keyword, "UTF-8");
			encodedKeyword.replace('+', ' ');
			Log.d("encodeKeyword", "keyword:" + encodedKeyword);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		Log.d(TAG, "Move URL: " + "http://m.enha.kr/wiki/" + encodedKeyword + "  keyword:" + keyword);
		AddDocTask addDocTask = new AddDocTask(urlMode + encodedKeyword, savedName, mContext);
		addDocTask.execute();
		Log.d(TAG, "tree:  MoveClick" + encodedKeyword);
	}

	public int getParentCount(EnhaDocument Doc) {
		int count = 0;

		return count;
	}

	// wiki인지 enha인지 체크한다.
	private void checkWiki() {
		if (MainActivity.MODE.equals(MODE_MIRROR)) {
			urlMode = URLs.URLEnhaWiki;
			urlModeFind = URLs.URLEnha + "/search/?q=";
		} else if (MainActivity.MODE.equals(MODE_WIKIPIDIA)) {
			urlMode = URLs.URLPidiaWiki;
			urlModeFind = URLs.URLPidiaSearchA;
			urlModeFind2 = URLs.URLPidiaSearchB;
		}

	}

	public void directMove(String url,String name) { 
		boolean networkConnect = NetworkCheck.networkCheck(mContext);
		if (!networkConnect) {
			Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			return;
		}
		
		AddDocTask addDocTask = new AddDocTask(url, name, mContext);
		addDocTask.execute(); 
	}
		 
}
