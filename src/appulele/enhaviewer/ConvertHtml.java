package appulele.enhaviewer;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;
import appulele.enhaviewer.strings.C;

public class ConvertHtml extends AsyncTask<Void, Void, Void> {
	/**
	 * a 태그의 class로 구분한다 wiki 를 포함하면 : Move external : 외부이미지 externalLink : 외부링크
	 * 
	 * a 태그의 href로 구분한다 #fn 을 포함하면 : 주석. 해당 a태그의 title을 출력해준다.
	 */
	private final String TAG = this.getClass().getSimpleName();
	private final boolean D = false;
	private final boolean MORBILE_PAGE = false;
	private final boolean NORMAL_PAGE = true;

	private Context mContext;
	private String convertHTML;

	private MainActivity mActivity;
	private ArrayList<LinkData> mLinkDataList;
	private String wikiURL;
	private EnhaDocument mSelectedDocument;
	private boolean hasParent = false;

	boolean curPageMode = NORMAL_PAGE;

	// AddDocTask이후 생성되는 데이터
	private WebView wv;
	private String tabName;

	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;
	private boolean blackMode = false;

	public ConvertHtml(Context context, String url) {
		Log.d(TAG, "tree:  AddDocText init, no Parent");
		mContext = context;
		mActivity = (MainActivity) context;
		wikiURL = url;

		sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		blackMode = mySharedPref.isBlackMode();
	}

	// /**
	// * 최상위 DOC
	// *
	// * @param url
	// * @param name
	// * @param context
	// */
	// public ConvertHtml(String url, String name, Context context) {
	// Log.d(TAG, "tree:  AddDocText init, no Parent");
	// mContext = context;
	// mActivity = (MainActivity) context;
	// this.tabName = name;
	// this.wikiURL = url.replace('+', ' ');
	// // Log.d(TAG, "parent has`t AddDocTask init:" + hasParent);
	//
	// }
	//
	// /**
	// * 자식 DOC
	// *
	// * @param selectedDoc
	// * @param url
	// * @param name
	// * @param context
	// */
	// public ConvertHtml(EnhaDocument selectedDoc, String url, String name,
	// Context context) {
	// Log.d(TAG, "tree:  AddDocText init, has Parent");
	// mContext = context;
	// mActivity = (MainActivity) context;
	// this.tabName = name;
	// this.wikiURL = url.replace('+', ' ');
	// this.mSelectedDocument = selectedDoc;
	// this.hasParent = true;
	// // Log.d(TAG, "parent has AddDocTask init:" + hasParent);
	// }

	@Override
	protected Void doInBackground(Void... params) {
		convertHTML = convertHTML(wikiURL);
		return null;
	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		EnhaDocument doc = mActivity.getCurSelDoc();
		doc.getWebView().loadData(convertHTML, "text/html; charset=utf-8", "utf-8");
		// Log.d(TAG, "hasParent is (" + hasParent + ")");
		// wv = createWebView(mContext, convertHTML);
		// EnhaDocument createdDocument = null;

		// createdDocument = new EnhaDocument(tabName, wv);
		// wv.addJavascriptInterface(WebViewJavaScriptObject.getObject(mContext,
		// createdDocument), "AppInterface");
		// createdDocument.setBtnTab(createTab(createdDocument,
		// mContext));createTab(createdDocument, mContext)
		// createdDocument.setTab(createTab(createdDocument, mContext));
		// mActivity.addDoc(createdDocument);

	}

	private String convertHTML(String wikiURL) {
		String originalHTML;
		Document doc = null;
		mLinkDataList = new ArrayList<LinkData>();

		// 해당 url의 HTML 받아옴
		try {
			doc = Jsoup.connect(wikiURL).get();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// 연결이 안되는 등 문제가 생기면 알림(타임아웃 인터넷 꺼놓음 등 )

		try {
			originalHTML = doc.html();
		} catch (Exception e) {
			return "<h1>서버의 응답이 없습니다, 다시 시도 해 주세요! </h1>";
		}

//		if (blackMode) {
//			originalHTML = originalHTML.replace(C.whiteModeA, C.blackModeA);
//			originalHTML = originalHTML.replace(C.whiteModeB, C.blackModeB);
//		}

		Elements elementLinks = doc.select("a[href]");
		// HTML내부의 모든 링크를 mLinkDataList에 추가
		for (Element link : elementLinks) {
			LinkData linkData = new LinkData(link);
			mLinkDataList.add(linkData);
		}

		// 링크 담아놓을 ArrayList선언
		ArrayList<LinkData> wikiLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> externalLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> externalLinkLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> footLinkDataList = new ArrayList<LinkData>();
		ArrayList<LinkData> findNextList = new ArrayList<LinkData>();
		ArrayList<LinkData> othersList = new ArrayList<LinkData>();

		// HTML 링크 클릭시 함수 동작
		for (int i = 0; i < mLinkDataList.size(); i++) {
			LinkData curLinkData = mLinkDataList.get(i);
			String linkClass = curLinkData.getLinkClass();
			String linkTitle = curLinkData.getTitle();
			String linkUrl = curLinkData.getUrl();

			if (linkClass.equals("wiki")) {
				wikiLinkDataList.add(curLinkData);
			} else if (linkClass.equals("external")) {
				externalLinkDataList.add(curLinkData);
			} else if (linkClass.equals("externalLink")) {
				externalLinkLinkDataList.add(curLinkData);
			} else if (linkUrl.contains("#fn")) {
				footLinkDataList.add(curLinkData);
			} else if (linkUrl.contains("search?q=")) {
				findNextList.add(curLinkData);
			} else {
				othersList.add(curLinkData);
			}
		}

		// Log
		if (D) {
			String listNameArray[] = new String[] { "wiki", "external", "externalLink", "foot", "others" };
			ArrayList<ArrayList<LinkData>> listArray = new ArrayList<ArrayList<LinkData>>();
			listArray.add(wikiLinkDataList);
			listArray.add(externalLinkDataList);
			listArray.add(externalLinkLinkDataList);
			listArray.add(footLinkDataList);
			listArray.add(othersList);

			for (int i = 0; i < listArray.size(); i++) {
				ArrayList<LinkData> curLinkDataList = listArray.get(i);
				String type = listNameArray[i];
				Log.d(TAG, "-- " + type + " Start --");
				for (int j = 0; j < curLinkDataList.size(); j++) {
					LinkData curLinkData = curLinkDataList.get(j);
					Log.d(TAG, type + " [" + j + "] : " + curLinkData.getTitle() + ", " + curLinkData.getUrl());
				}
				Log.d(TAG, "-- " + type + " End --");
			}
		}

		/**
		 * href="/wiki/([^\"]+)\""; 인 모든 링크주소 한 번에 바꾸기 ex)
		 * href="/wiki/starcraft" ->
		 * href="javascript:AppInterface.wiki("starcraft")"
		 */
		String wikiRegex = "href=\"/wiki/([^\"]+)\"";
		originalHTML = originalHTML.replaceAll(wikiRegex, "href=\"" + "javascript:AppInterface.wiki(" + "\'$1\'" + ")\"");

		/**
		 * href="/search?q=(keyword)&p=(page_num) href="/search?q=Korea&p=2 ->
		 * href="javascript:AppInterface.page("/search?q=Korea&p=2")
		 */
		String pageRegex = "href=\"/search([^\"]+)\"";
		originalHTML = originalHTML.replaceAll(pageRegex, "href=\"" + "javascript:AppInterface.page(" + "\'$1\'" + ")\"");

		/**
		 * 외부 이미지 링크 함수로 바꾸기
		 * */
		for (int i = 0; i < externalLinkDataList.size(); i++) {
			// 외부 주소의 - 문자를 html형으로 변환
			String url = externalLinkDataList.get(i).getUrl();
			String urlConvert = externalLinkDataList.get(i).getUrl().replace("-", "&#45;");// .replace("(",
																							// "&#40;").replace(")"," &#41;");
			originalHTML = originalHTML.replaceAll("href=\"" + url, "href=\"" + "javascript:AppInterface.external(\'" + urlConvert + "\')");
			Log.d("externalLink", "externalLink URL CONVERT:" + urlConvert);
		}

		/**
		 * 주석 함수로 바꾸기
		 * */
		for (int i = footLinkDataList.size() - 1; i >= 0; i--) {
			String url = footLinkDataList.get(i).getUrl();
			// String형의 특수문자를 HTML의 특수 문자로 변환한다.
			String text = footLinkDataList.get(i).getTitle().replace("\"", "&quot;").replace("\'", "&quot;");
			Log.d("	footLinkData Title : ", "name: " + footLinkDataList.get(i).getText() + "  title: " + text);
			Log.d("	footLinkData Title : ", "url: " + footLinkDataList.get(i).getUrl());
			try {
				originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.foot(\'" + text + "\')");

			} catch (Exception e) {
				Log.d(TAG, "error");
			}
		}

		/**
		 * 외부 링크 웹뷰로 띄우기
		 * */

		for (int i = 0; i < externalLinkLinkDataList.size(); i++) {
			String url = externalLinkLinkDataList.get(i).getUrl();
			String text = externalLinkLinkDataList.get(i).getTitle();
			originalHTML = originalHTML.replaceAll(url, "javascript:AppInterface.externalLink(\'" + url + "\')");
		}
		// HTML내부의 필요없는 부분 지우기
		// 일반화면 and 모바일 화면
		if (curPageMode == MORBILE_PAGE) {
			originalHTML = originalHTML.replace("<div class=\"toolbar\">", "<div class=\"toolbar\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div class=\"toolbox\">", "<div class=\"toolbox\" style=\"visibility: hidden; display:none;>");
		} else if (curPageMode == NORMAL_PAGE) {
			originalHTML = originalHTML.replace("<div class=\"boilerplate\">", "<div class=\"boilerplate\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div class=\"disclaimer\">", "<div class=\"disclaimer\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div class=\"search-box\">", "<div class=\"search-box\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div class=\"search-form\">", "<div class=\"search-form\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div id=\"_toolbox\">", "<div id=\"_toolbox\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div id=\"_toolbox\">", "<div id=\"_toolbox\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<div class=\"wrapper\">", "<div class=\"wrapper\" style=\"visibility: hidden; display:none;>");
			originalHTML = originalHTML.replace("<p class=\"character\">", "<p class=\"character\"  style=\"visibility: hidden; display:none;>");

		}

		/**
		 * 다음 페이지 링크 기존 웹뷰로 열기
		 */
		// <a href="/search?q=Korea&amp;p=10">10</a>
		// /search?q=Korea&amp;p=2
		// "/search?q=[검색어]&amp;p=[n번째]" -> "javascript:AppInterface.foot(\'" +
		// /search?q=[검색어]&amp;p=[n번째] + "\')"
		// originalHTML = originalHTML.replaceAll("/search?q=Korea&p=10",
		// "http://www.naver.com");

		// for (int i = 0; i < findNextList.size(); i++) {
		// // 외부 주소의 - 문자를 html형으로 변환
		// String url = findNextList.get(i).getUrl();
		// // url = url.replace("/&", "/&amp;");
		// // url = url.replace("?","&#63;");
		//
		// int index = i+2;
		// // originalHTML = originalHTML.replaceAll(url,
		// "javascript:AppInterface.nextPage(\'" + tabName + ","+index+"\')");
		// originalHTML = originalHTML.replaceAll(url,
		// "http://mirror.enha.kr"+url);
		//
		// // i = i+2
		// Log.d(TAG,"findName  tabName"+tabName + " ,   i :"+i +
		// ", url :"+url);
		// }
		return originalHTML;

	}

	private WebView createWebView(Context mContext, String convertHTML) {
		wv = new WebView(mContext);
		wv.loadData(convertHTML, "text/html; charset=utf-8", "utf-8");
		// 생성된 webView를 새로이 EnhaDoc를 생성해서 부모로 Link를 정해준다.
		Log.d(TAG, "hasParent webView");
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		ScrollView sv = new ScrollView(mContext);
		sv.setLayoutParams(params);
		// 웹뷰로 js alert보기위한 설정
		// fix3 webView 에 태그 달기

		wv.getSettings().setJavaScriptEnabled(true);
		wv.setWebChromeClient(new WebChromeClient());
		// 웹뷰로 새창 열기
		wv.setOnTouchListener(mActivity);
		wv.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// return false;
				try {
					URL urlObj = new URL(url);
					Intent intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(url));
					mActivity.startActivity(intent);
					return true;

				} catch (Exception e) {
					e.printStackTrace();
				}
				return false;
			}
		});

		return wv;
	}

}
