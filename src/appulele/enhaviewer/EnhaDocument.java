package appulele.enhaviewer;

import java.util.ArrayList;

import android.R.array;
import android.content.Context;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
/**
 * 엔하 문서를 탭으로 보여주기 위해 가져야 할 정보를 모아놓은 클래스
 * @author sanghyeon
 *
 */
public class EnhaDocument {
	private static final String TAG = "EnhaDocument";
	private static final boolean D = true;
	EnhaDocument parent;
	ArrayList<EnhaDocument> childrenList;
	private WebView webView;
	private LinearLayout layoutTab;
	private Button btnTab;
	private Button btnClose;
	private String curTabName = "";
	private LinearLayout  tab;
	private String url;
	public EnhaDocument(String name, WebView wv) {
		Log.d(TAG, "tree:  EnhaDocument init, no Parent");
		this.parent = null;
		this.curTabName = name;
		this.webView = wv;
		childrenList = new ArrayList<EnhaDocument>();
	}

	public EnhaDocument(String name, WebView wv, LinearLayout tab) {
		Log.d(TAG, "tree:  EnhaDocument init, no Parent");
		this.parent = null;
		this.curTabName = name;
		this.webView = wv;
		childrenList = new ArrayList<EnhaDocument>();
		this.tab = tab;
		this.btnTab = (Button) tab.getChildAt(0);
		
	}

	public EnhaDocument(EnhaDocument parentDoc, String name, WebView wv) {
		Log.d(TAG, "tree:  EnhaDocument init, hasParent");
		this.parent = parentDoc;
		this.curTabName = name;
		this.webView = wv;
		childrenList = new ArrayList<EnhaDocument>();
	}

	/**
	 * 조상의 숫자를 불러온다
	 * 
	 * @return
	 */
	public int getAncestorCount() {
		int count = 1;
		ArrayList<EnhaDocument> childList = getChildren();
		for (EnhaDocument child : childList) {
			count += child.getAncestorCount();
		}

		return count;
	}

	public boolean hasParent() {
		boolean value = false;
		if (parent != null) {
			value = true;
		}
		return value;
	}

	public boolean hasChild() {
		boolean value = false;
		if (parent != null) {
			value = true;
		}
		return value;
	}

	public EnhaDocument getParent() {
		return parent;
	}

	public void setParent(EnhaDocument parentDoc) {
		this.parent = parentDoc;
	}

	public ArrayList<EnhaDocument> getChildren() {
		return childrenList;
	}

	/**
	 * 해당 Document를 포함한 모든 자식들을 순서대로 리스트로 반환
	 * 
	 * @return
	 */
	public ArrayList<EnhaDocument> getDocList() {
		ArrayList<EnhaDocument> returnList = new ArrayList<EnhaDocument>();
		returnList.add(this);

		for (EnhaDocument doc : childrenList) {
			Log.d(TAG, "Child doc name is " + doc.getCurTabName());
			ArrayList<EnhaDocument> subDocList = doc.getDocList();
			returnList.addAll(subDocList);
		}
		return returnList;
	}

	public WebView getWebView() { 
		webView.setTag(this);
		return webView;
	}

	public void setWebView(WebView webView) {
		this.webView = webView;
	}

	public String getCurTabName() {
		return curTabName;
	}

	public void setCurTabName(String curTabName) {
		this.curTabName = curTabName;
	}

	public Button getBtnTab() {
		return btnTab;
	}

	public void setBtnTab(Button btnTab) {
		this.btnTab = btnTab;
	}

	public Button getBtnClose() {
		return btnClose;
	}

	public void setBtnClose(Button btnClose) {
		this.btnClose = btnClose;
	}

	public LinearLayout getLayoutTab() {
		return layoutTab;
	}

	public void setLayoutTab(LinearLayout layoutTab) {
		this.layoutTab = layoutTab;
		layoutTab.addView(btnClose);
		layoutTab.addView(btnTab);
	}

	public int getParentCount() {
		int count = 0;
		// int count = 1;
		if (hasParent()) {
			count++;
			count += parent.getParentCount();
		}

		Log.d(TAG, "" + getCurTabName() + "의 조상카운트는 " + count);
		return count;
	}

	/**
	 * 인자로 온 DOCUMENT를 childList에서 삭제한다.
	 * 
	 * @param beRemovedDoc
	 */
	public void removeChild(EnhaDocument beRemovedDoc) {
		childrenList.remove(beRemovedDoc);
	}

	/**
	 * children들의 parent를 this의  parent로 바꾼다. 
	 */
	public void changeChildrensParent() {
		for (EnhaDocument child : childrenList) {
			child.setParent(parent);
		}
	}
	
	/**
	 * children의 parent를 doc로 바꿔준다.
	 * @param doc
	 */
	public void changeChildrensParent(EnhaDocument doc) {
		for (EnhaDocument child : childrenList) {
			child.setParent(doc);
		}
	}

	/**
	 * 현재 Doc의 childList 의 일부인, addDoc의 ChildList를 addDoc의 position에 추가한다.
	 *. addDoc의 childList의 child들은 부모를 현재 Doc로 재 설정한다.
	 * 
	 * @param addDoc
	 * @return
	 */
	public void addChildren(EnhaDocument addDoc) {
		int position = 0;
		// addDoc의 position을 구한다. 
		for (int i = 0; i < childrenList.size(); i++) {
			EnhaDocument child = childrenList.get(i);
			if (child != addDoc)
				position++;
			else
				break;
		}
		// addDoc의 childList들의 부모를 this Doc로 설정
		addDoc.changeChildrensParent(this);
		ArrayList<EnhaDocument> addDocChildren = addDoc.getChildren();
		childrenList.addAll(position, addDocChildren); 
	
		if(D){
			for (int i = 0; i < childrenList.size(); i++) {
				Log.d(TAG, "childrenList  : "+childrenList.get(i).getCurTabName());
			}
			
		}
	}

	public void setTab(LinearLayout tab) {
		this.tab = tab;
		this.btnTab = (Button)tab.getChildAt(0);
	}
	
	public LinearLayout getTab() {
		return tab;
		
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
