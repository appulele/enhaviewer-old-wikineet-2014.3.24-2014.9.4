package appulele.enhaviewer;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;  

/**
 * 외부 링크 새 다이얼로그로 보여주기
 * @author sanghyeon
 *
 */
public class ExternalLinkNewDialog { 
	@SuppressWarnings("deprecation")
	public static void newDialog(Context context, String url) {
		// youtube 링크면 동영상 재생 // 아직 미 구현 (youtube api적용 해야 되나...)
		MainActivity mainActivity = (MainActivity) context;
		boolean networkConnect = NetworkCheck.networkCheck(context);
		if (networkConnect) {
	 
			LayoutInflater inflater = LayoutInflater.from(mainActivity);
			final WebView webViewer = (WebView) inflater.inflate(R.layout.webviewer, null);
			WebSettings webSettings = webViewer.getSettings();
			webSettings.setPluginState(WebSettings.PluginState.ON);
			// webSettings.setJavaScriptEnabled(true);
			webSettings.setUseWideViewPort(true);
			webSettings.setLoadWithOverviewMode(true);
			webViewer.setWebChromeClient(new WebChromeClient());
			webViewer.loadUrl("http://www.ygfamily.com/artist/About.asp?LANGDIV=K&ATYPE=2&ARTIDX=15");

			new AlertDialog.Builder(context).setMessage("외부 링크").setView(webViewer).setPositiveButton("닫기", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
				}
			}).show();
		} else {
			Toast.makeText(context, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
		}
	}
}