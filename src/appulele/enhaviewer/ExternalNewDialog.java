package appulele.enhaviewer;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.VideoView;  

/**
 * 받은 URL에 YOUTUBE가 포함되어 있으면 동영상 외부 링크로 유투브 동영상을 보여준다. 이외에 다른 URL인 경우 외부 이미지로
 * 생각하고 이미지뷰를 이용하여 보여준다. 
 * */
public class ExternalNewDialog {

	@SuppressWarnings("deprecation")
	public static void newDialog(Context context, String url) {
		// youtube 링크면 동영상 재생 // 아직 미 구현 (youtube api적용 해야 되나...)
		MainActivity mainActivity = (MainActivity) context;
		boolean networkConnect = NetworkCheck.networkCheck(context);
		if (networkConnect) { 
			if (url.contains("youtube")) { 
				Uri uri = Uri.parse(url);
				Intent intent = new Intent(Intent.ACTION_VIEW, uri);
				mainActivity.startActivity(intent);
			}
			// 유투브가 아니면 이미지 출력
			else {
				LayoutInflater inflater = LayoutInflater.from(mainActivity);
				final ImageView imageView = (ImageView) inflater.inflate(R.layout.imageviewer, null);
				final AlertDialog.Builder alertDlgBuilder = new AlertDialog.Builder(context);
				try {
					URL urlConvert = new URL(url);
					URLConnection conn = urlConvert.openConnection();
					conn.connect();
					BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
					Bitmap bm = BitmapFactory.decodeStream(bis);
					bis.close();
					imageView.setImageBitmap(bm);
				} catch (Exception e) {
				}
				// 이미지 뷰 클릭시 alertDlg 닫기
				imageView.setOnClickListener(new ImageView.OnClickListener() {
					@Override
					public void onClick(View arg0) {

					}
				});
				alertDlgBuilder.setMessage(url).setView(imageView).setPositiveButton("닫기", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				}).show();
			}
		}else{
			Toast.makeText(context, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show(); 
		}//network Connect== false면 
	}

}
