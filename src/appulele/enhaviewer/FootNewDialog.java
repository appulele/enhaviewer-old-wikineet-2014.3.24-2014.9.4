package appulele.enhaviewer;

import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebSettings.PluginState;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.VideoView;

/**
 * 주석 alertDialog 새창 띄우기
 * */
public class FootNewDialog {

	public static void newDialog(Context context, String text) {

		new AlertDialog.Builder(context).setMessage(text)
				.setPositiveButton("닫기", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
					}
				}).show();

	}

}
