package appulele.enhaviewer;

import org.jsoup.nodes.Element;

/**
 * JSOUP을 통해 구한 정보를 보관하는 클래스 
 * @author sanghyeon
 *
 */
public class LinkData {
	private String text;
	private String foot;
	private String url;
	private String id;
	private String title;
	private String linkClass;
	private String linkDown;
	
	public LinkData(Element link) {
		this.text = link.text();
		this.id = link.id();
		this.url = link.attr("href");
		this.title = link.attr("title");
		this.linkClass = link.attr("class");
		this.foot =  link.attr("title").replace("wiki:", "");
		this.setLinkDown(link.ownText());
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLinkClass() {
		return linkClass;
	}

	public void setLinkClass(String linkClass) {
		this.linkClass = linkClass;
	}

	public String getFoot() {
		return foot;
	}

	public void setFoot(String foot) {
		this.foot = foot;
	}

	public String getLinkDown() {
		return linkDown;
	}

	public void setLinkDown(String linkDown) {
		this.linkDown = linkDown;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	
	
}
