package appulele.enhaviewer;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;  

import com.mocoplex.adlib.AdlibConfig;

public class LoadingActivity extends BaseActivity {
	private final String TAG = this.getClass().getSimpleName();
	boolean Debug = true;
	String packageName;
	String idAdlibr;

	/** Called when the activity is first created. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.imageviewer);
		packageName = getPackageName();
		idAdlibr = getResources().getString(R.string.ad_adlibr);

		initAds();
		Intent intent = new Intent(LoadingActivity.this, MainActivity.class);
		// startActivity(intent);
		startActivityForResult(intent, 0);

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode==0){
			finish();
		}
	}

	// 광고 초기화 함수
	protected void initAds() {
		if (Debug) {
			Log.d(TAG, "--initAds Start--");
			// Log.d(TAG, "-INMOBI : " + packageName +
			// ".ads.SubAdlibAdViewInmobi");
			Log.d(TAG, "-ADAM : " + packageName + ".ads.SubAdlibAdViewAdam");
			Log.d(TAG, "-CAULY : " + packageName + ".ads.SubAdlibAdViewCauly");
			Log.d(TAG, "--initAds End--");
		}

		// AdlibConfig.getInstance().bindPlatform("INMOBI", packageName +
		// ".ads.SubAdlibAdViewInmobi");
		// 쓰지 않을 광고플랫폼은 삭제해주세요.
	
		// AdlibConfig.getInstance().bindPlatform("ADMOB","lhy.undernation.ads.SubAdlibAdViewAdmob"); 
		// AdlibConfig.getInstance().bindPlatform("TAD","lhy.undernation.ads.SubAdlibAdViewTAD");
		// AdlibConfig.getInstance().bindPlatform("NAVER","lhy.undernation.ads.SubAdlibAdViewNaverAdPost");
		// AdlibConfig.getInstance().bindPlatform("SHALLWEAD","lhy.undernation.ads.SubAdlibAdViewShallWeAd");
		
		AdlibConfig.getInstance().setAdlibKey(idAdlibr);
		AdlibConfig.getInstance().bindPlatform("ADAM", packageName + ".ads.SubAdlibAdViewAdam");
		AdlibConfig.getInstance().bindPlatform("CAULY", packageName + ".ads.SubAdlibAdViewCauly");
	}
}
