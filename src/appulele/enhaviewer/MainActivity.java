package appulele.enhaviewer;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Random;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import appulele.enhaviewer.dialogs.BrightDlg;
import appulele.enhaviewer.dialogs.ExSettingsDlg;
import appulele.enhaviewer.dialogs.FontSizeDlg;
import appulele.enhaviewer.dialogs.InDocFinderDlg;
import appulele.enhaviewer.dialogs.SettingDlg;
import appulele.enhaviewer.downloadimage.DownImageTask;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;
import appulele.enhaviewer.sqlite.MySQLiteHandler;
import appulele.enhaviewer.strings.C;

import com.androidquery.AQuery;

/**
 * 동작 1. 이동 검색 클릭시 CreateDoc->CreateTab 동작 2. CreateTab->AddDoc
 * 
 * */
@SuppressLint("NewApi")
public class MainActivity extends BaseActivity implements OnClickListener, OnTouchListener, OnKeyListener, OnLongClickListener {
	private final String MODE_MIRROR = "MIRROR";
	private final String MODE_WIKIPIDIA = "WIKIPIDIA";
	public static String MODE = "";
	private final boolean D = false;
	private final String TAG = this.getClass().getSimpleName();
	// 부모를 가지고 있으면 1, 없으면 최상위로 0
	public static int TYPE_HIGHIST = 0;
	public static int TYPE_HAS_PARENTS = 1;
	private EditText etMain;
	LinearLayout llMain;
	LinearLayout llMainFind;
	LinearLayout llMainAddWebView;
	LinearLayout llTabMid;
	LinearLayout llTabNavi;
	FrameLayout frame;
	AlertDialog mDlg;
	HorizontalScrollView horiTab;
	Refresh refresh;
	int curFindNum = 0;
	private DrawerLayout mDrawerLayout;
	private ArrayList<EnhaDocument> topDocumentList;
	ArrayList<Button> topBtnList;
	ViewFlipper viewFlipper;

	// String urlMode = URLs.URLEnhaWiki;
	Button btnFind;
	Button btnMove;
	Button btnSelected;
	private Button btnWIKI;
	// 모바일 페이지로 볼지, 일반 페이지로 볼지 설정
	private static final boolean MORBILE_PAGE = false;
	private static final boolean NORMAL_PAGE = true;
	boolean curPageMode = MORBILE_PAGE;
	private int mPreTouchPosX = 0;
	// SharedPreferences prefNames;
	// 현재 선택된 DOC
	private EnhaDocument curSelDoc;
	ClickMoveButton clickMove;
	// DB처리
	MySQLiteHandler findNameSaveHandler;
	// sharedPrefrence
	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;
	private boolean callBeforeTabs = true;
	private boolean sendFindNames = true;
	private int fontSize = 13;
	private int sensitivity = 210;
	private int bright = 60;

	private boolean blackMode = false;
	private boolean newFootDlg = true;
	private boolean directMove = true;
	private boolean createMovieButton = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inits();

	}

	/**
	 * 현재 Document 의 position을 불러온다.
	 * 
	 * @param curDoc
	 * @return count
	 */
	public int getDocPosition(EnhaDocument selDoc) {
		int position = 0;
		ArrayList<EnhaDocument> list = new ArrayList<EnhaDocument>();
		for (EnhaDocument topDoc : getTopDocumentList()) {
			// 선택된 최상위 문서와 그 문서의 자식 리스트를 순서대로 불러온다.
			// 현재 Document와 for문의 document가 동일하지 않으면 count +1
			list = topDoc.getDocList();
			boolean flag = false;
			for (EnhaDocument child : list) {
				if (child != selDoc) {
					position++;
				} else {
					flag = true;
					break;
				}
			}
			if (flag)
				break;

		}
		return position;
	}

	/**
	 * sideDocument에서 포지션을 불러온다.
	 * 
	 * @param selDoc
	 * @return
	 */
	@Override
	public void onClick(View btn) {
		btnSelected = (Button) btn;
		ClickMoveButton clickMove = new ClickMoveButton(mContext);
		int position = 0;
		switch (btnSelected.getId()) {
		case R.id.btnMainFind:// 찾기 버튼을 눌렀을 때 검색어가 비어있지 않으면 검색
			MODE = MODE_MIRROR;
			clickMove.find(getCurSelDoc());
			break;

		case R.id.btnMainMove:// 이동 버튼을 눌렀을 때 검색어가 비어있지 않으며 이동
			MODE = MODE_MIRROR;
			clickMove.move(getCurSelDoc());
			break;

		case R.id.btnMainWIKIPIDIA:// 이동 버튼을 눌렀을 때 검색어가 비어있지 않으며 이동
			MODE = MODE_WIKIPIDIA;
			clickMove.find(getCurSelDoc());
			break;

		case R.id.buttonDlgCancle:
			mDlg.cancel();
			break;
		case R.id.buttonDlgCloseApp:

			finish();

			break;
		case R.id.buttonDlgCloseTab:
			if (llTabMid.getChildCount() > 0)
				deleteDoc(getCurSelDoc());

			mDlg.cancel();
			break;

		default:
			setCurSelDoc((EnhaDocument) btnSelected.getTag());
			refresh.refreshTabLayout(getCurSelDoc());
			int positionVFlipper = viewFlipper.getDisplayedChild();
			horiTab.scrollTo(220 * (positionVFlipper), 0);
			break;
		}
	}

	/**
	 * 닫기 버튼 생성(안쓰기로 결정)
	 * 
	 * @param selDoc
	 * @return
	 */
	public ImageButton CreateCloseBtn(EnhaDocument selDoc) {
		if (D)
			Log.d(TAG, "AA CreateCloseBtn");
		int position = getDocPosition(selDoc);
		LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		LinearLayout tab = null;
		tab = (LinearLayout) inflater.inflate(R.layout.tab, tab);
		ImageButton ibClose = (ImageButton) tab.findViewById(R.id.ibTabClose);

		ibClose.setTag(selDoc);
		ibClose.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View btnClose) {
				EnhaDocument doc = (EnhaDocument) btnClose.getTag();
				if (D)
					Log.d(TAG, "AA in OnClick`s delete doc");
				deleteDoc(doc);
			}
		});

		Handler handlerTimerViewRefresh = new Handler();
		handlerTimerViewRefresh.postDelayed(new Runnable() {
			public void run() {
				int positionVFlipper = viewFlipper.getDisplayedChild();
				horiTab.scrollTo(220 * (positionVFlipper + 1), 0);
			}
		}, 110);
		return ibClose;
	}

	/**
	 * 선택한 selDoc를 EnhaDocument 트리구조에서 찾아 삭제하고, ViewFlipper에서도 삭제한다. 1. 부모가 있으면
	 * 부모의 childList의 position에 자식들을 추가시키고 selDoc는 삭제한다. 2. 부모가 없으면 topList의 현재
	 * 위치에 자식들을 추가시키고 selDoc는 삭제한다.
	 * 
	 * @param selDoc
	 */
	protected void deleteDoc(EnhaDocument selDoc) {
		if (D)
			Log.d(TAG, "AA DeleteDoc");
		// ViewFlipper에서 뷰 삭제
		int position = getDocPosition(selDoc);
		viewFlipper.removeViewAt(position);
		Log.d(TAG, "positions:" + position);
		if (position < viewFlipper.getChildCount()) {
			if (viewFlipper.getChildCount() > position) {
				View view = viewFlipper.getChildAt(position);
				setCurSelDoc((EnhaDocument) view.getTag());
			}
		} else if (viewFlipper.getChildCount() == position && position > 0) {
			View view = viewFlipper.getChildAt(position - 1);
			setCurSelDoc((EnhaDocument) view.getTag());
		}

		// DB에서 삭제
		findNameSaveHandler.delete(selDoc.getCurTabName());
		// 부모가 있으면
		if (selDoc.hasParent()) {
			EnhaDocument parent = selDoc.getParent();
			parent.addChildren(selDoc);
			parent.removeChild(selDoc);
			// 부모가 없으면 topDocList의 selDoc의 position에 selDoc의 children을 parent를
			// null로 해주고 추가한다.
		} else {
			selDoc.changeChildrensParent(null);
			int topPosition = 0;
			for (int i = 0; i < getTopDocumentList().size(); i++) {
				EnhaDocument topDoc = getTopDocumentList().get(i);
				if (topDoc != selDoc) {
					topPosition++;
				} else {
					break;
				}
			}
			getTopDocumentList().remove(selDoc);
			getTopDocumentList().addAll(topPosition, selDoc.getChildren());
		}
		// 리스트에서 삭제하고 뷰 플리퍼에서 삭제 한 이후, 새로이 refresh하고 CloseBtn추가

		refresh.refreshTabLayout(getCurSelDoc());
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		sensitivity = mySharedPref.getSensitivity();
		boolean value = false;
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			mPreTouchPosX = (int) event.getX();
		} else if (event.getAction() == MotionEvent.ACTION_UP) {
			int nTouchPosX = (int) event.getX();
			if (Math.abs(mPreTouchPosX - nTouchPosX) > sensitivity) {
				if (nTouchPosX < mPreTouchPosX) {
					MoveNextView();
					Log.d(TAG, "viewFlipper ontouch next");
					value = true;
				} else if (nTouchPosX > mPreTouchPosX) {
					MovePreviousView();
					Log.d(TAG, "viewFlipper ontouch before");
					value = true;
				}
			} else if (Math.abs(mPreTouchPosX - nTouchPosX) < 1) {
				// WebView wv = (WebView) v;
				// final WebView.HitTestResult result = wv.getHitTestResult();
				//
				// if (result != null) {
				// switch (result.getType()) {
				// case WebView.HitTestResult.IMAGE_TYPE:
				// LayoutInflater inflater = LayoutInflater.from(this);
				//
				// final ImageView imageView = (ImageView)
				// inflater.inflate(R.layout.imageviewer, null);
				// // webViewer.loadData(downloadImg(result.getExtra()),
				// "text/html; charset=UTF-8", null);
				//
				// final AQuery aq = new AQuery(imageView);
				// aq.id(R.id.ivDialog).image(result.getExtra(), false,true);
				//
				// // webViewer.loadData(downloadImg(result.getExtra()),
				// // "text/html; charset=UTF-8", null);
				// Log.i(TAG, "IMAGE_TYPE" + String.valueOf(result.getType()) +
				// result.getExtra());
				// new
				// AlertDialog.Builder(mContext).setMessage("이미지를 다운로드 하기(BETA)").setView(imageView).setPositiveButton("저장",
				// new DialogInterface.OnClickListener() {
				// @Override
				// public void onClick(DialogInterface dialog, int whichButton)
				// {
				// String dirPath = C.PATH_EXTERNAL + File.separator +
				// "EnhaViewer" +
				// File.separator;
				// Random random = new Random();
				//
				// String filePath = dirPath + "EnhaViewer" +random.nextInt();
				// // String filePath = dirPath + "EnhaViewer_" +
				// result.getExtra();
				// Log.d(TAG,"filePathA"+filePath);
				// DownImageTask task = new DownImageTask(aq, dirPath, filePath,
				// result.getExtra(), mContext);
				// task.execute();
				// }
				// }).setNegativeButton("닫기", new
				// DialogInterface.OnClickListener() {
				//
				// @Override
				// public void onClick(DialogInterface dialog, int which) {
				// }
				// }).show();
				//
				// // WebView wvs = new WebView(mContext);
				// // wvs.
				// // wvs.loadUrl(html);
				// break;
				// default:
				// break;
				// }
				// }
				return false;
			}
		} else {
			value = false;
		}
		int positionVFlipper = viewFlipper.getDisplayedChild();
		horiTab.scrollTo(220 * (positionVFlipper), 0);
		return value;
	}

	/**
	 * url을 입력하면 url을 다운받는 HTML 문서를 만든다.
	 * 
	 * @param url
	 * @return
	 */
	public String downloadImg(String url) {
		String str = "<a href=" + url + " download=\"ImageName\" title=\"ImageName\"> <img src=" + url + " alt=\"ImageName\"></a>";
		return str;
	}

	/**
	 * ViewFlipper 다음 뷰 이동
	 */
	private void MoveNextView() {
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.appear_from_right));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.disappear_to_left));
		viewFlipper.showNext();
		int position = viewFlipper.getDisplayedChild();
		setCurSelDoc((EnhaDocument) viewFlipper.getChildAt(position).getTag());
		refresh.refreshTabLayout(position);
		EnhaDocument doc = (EnhaDocument) viewFlipper.getChildAt(position).getTag();
		// CreateCloseBtn(doc);
	}

	/**
	 * ViewFlipper 이전 뷰 이동
	 */
	private void MovePreviousView() {
		viewFlipper.setInAnimation(AnimationUtils.loadAnimation(this, R.anim.appear_from_left));
		viewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this, R.anim.disappear_to_right));
		viewFlipper.showPrevious();
		int position = viewFlipper.getDisplayedChild();
		setCurSelDoc((EnhaDocument) viewFlipper.getChildAt(position).getTag());
		refresh.refreshTabLayout(position);
		// EnhaDocument doc = (EnhaDocument)
		// viewFlipper.getChildAt(position).getTag();
		Log.d(TAG, "hori:" + horiTab.getWidth());
		Log.d(TAG, "hori:" + llTabMid.getWidth());
	}

	public EditText getEtMain() {
		return etMain;
	}

	/**
	 * 최상위 탭 리스트를 기준으로 create된 Doc 추가
	 * 
	 * @param createDoc
	 */
	public void addDoc(EnhaDocument createDoc, String url) {
		getTopDocumentList().add(createDoc);
		viewFlipper.setAnimationCacheEnabled(false);
		viewFlipper.addView(createDoc.getWebView());
		viewFlipper.setAnimationCacheEnabled(true);
		setCurSelDoc(createDoc);

		refresh.refreshTabLayout(getCurSelDoc());
		int positionVFlipper = viewFlipper.getDisplayedChild();
		horiTab.scrollTo(220 * (positionVFlipper), 0);

		// 데이터 저장
		String myName = createDoc.getCurTabName();

		findNameSaveHandler.insert(url, myName);
		findNameSaveHandler.close();
		// setCurSelDoc(createDoc);
		// refresh.refreshTabLayout(getDocPosition(createDoc));

		if (D)
			Toast.makeText(mContext, "CURSELDOC position: " + getDocPosition(getCurSelDoc()) + "\n curSelDoc = " + getCurSelDoc().getCurTabName(), Toast.LENGTH_SHORT).show();

	}

	/**
	 * 현재 selectedDoc를 기준으로 createDoc 추가.
	 * 
	 * @param createDoc
	 * @param curSelectedDoc
	 */
	public void addDoc(EnhaDocument createDoc, EnhaDocument curSelectedDoc, String docURL) {
		curSelectedDoc.getChildren().add(createDoc);
		int position = getDocPosition(createDoc);// 현재 Doc의 position
		Log.d(TAG, "curSelectDoc Position : " + getDocPosition(curSelectedDoc) + " just  createDoc position::" + position);
		viewFlipper.setAnimationCacheEnabled(false);
		viewFlipper.addView(createDoc.getWebView(), position);
		viewFlipper.setAnimationCacheEnabled(true);

		// 링크 클릭시 링크로 화면전환/기존화면 유지
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		directMove = mySharedPref.isDirectMove();
		if (directMove)
			refresh.refreshTabLayout(createDoc);
		else
			refresh.refreshTabLayout(curSelectedDoc);
		//
		// 데이터 저장
		String myName = createDoc.getCurTabName();
		// String parentName = createDoc.getParent().getCurTabName();
		findNameSaveHandler.insert(docURL, myName);
		if (D)
			Toast.makeText(mContext, "position: " + position, Toast.LENGTH_SHORT).show();
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	private void inits() {
		showAd = true;
		// requestWindowFeature(Window.FEATURE_NO_TITLE);
		ActionBar actionBar = getActionBar();
		actionBar.hide();
		setContentView(R.layout.activity_main);

		// 모드 설정하기(미러 모드)
		MODE = MODE_MIRROR;

		// MODE = MODE_WIKIPIDIA;
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

		setTopDocumentList(new ArrayList<EnhaDocument>());
		topBtnList = new ArrayList<Button>();
		boolean isChecked = NetworkCheck.networkCheck(mContext);
		Log.d(TAG, "네트워크 상태는 :" + isChecked);
		horiTab = (HorizontalScrollView) findViewById(R.id.horizontalScrollTab);
		llMain = (LinearLayout) findViewById(R.id.linearLayoutMain);
		llMainFind = (LinearLayout) findViewById(R.id.linearLayoutMainFind);
		llMainAddWebView = (LinearLayout) findViewById(R.id.linearLayoutMainAddWebView);
		llTabMid = (LinearLayout) findViewById(R.id.linearLayoutViewTab);
		llTabNavi = (LinearLayout) findViewById(R.id.llTNaviab);
		frame = (FrameLayout) findViewById(R.id.content_frame);
		etMain = ((EditText) findViewById(R.id.eTMainFind));
		// etMain.setText("옥상달빛");
		btnFind = (Button) findViewById(R.id.btnMainFind);
		btnFind.setOnClickListener(this);
		btnMove = (Button) findViewById(R.id.btnMainMove);
		btnMove.setOnClickListener(this);
		btnWIKI = (Button) findViewById(R.id.btnMainWIKIPIDIA);
		btnWIKI.setOnClickListener(this);

		btnSelected = new Button(mContext);
		viewFlipper = (ViewFlipper) findViewById(R.id.viewFlipperWebView);
		viewFlipper.setOnTouchListener(this);
		WebView wv = new WebView(mContext);

		refresh = new Refresh(mContext);
		setCurSelDoc(new EnhaDocument("FrontPage", wv));
		getCurSelDoc().setCurTabName("Front Page");
		getCurSelDoc().setTab(AddDocTask.createTab(getCurSelDoc(), mContext));
		// 이전 탭 불러 오기 서버에 검색 보내기 체크박스
		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		callBeforeTabs = mySharedPref.isCallSavedTabs();
		sendFindNames = mySharedPref.isSendFindNames();
		createMovieButton = mySharedPref.isCreateMovieBtn();
		fontSize = mySharedPref.getFontSize();
		sensitivity = mySharedPref.getSensitivity();
		blackMode = mySharedPref.isBlackMode();
		newFootDlg = mySharedPref.isNewFootDlg();
		directMove = mySharedPref.isDirectMove();
		Log.d(TAG, "dafault ddirectMove:" + directMove);
		setDefaultPageColor(blackMode);

		// 데이터베이스 연동
		findNameSaveHandler = MySQLiteHandler.open(getApplicationContext());
		clickMove = new ClickMoveButton(mContext);
		if (findNameSaveHandler == null) {
			Toast.makeText(mContext, "서버쪽에 문제가 있습니다. 다시 확인 후에 시도해 주세요.", Toast.LENGTH_LONG).show();
		} else {
			// checkerFirst가 끝까지 true면 FrontPage 불러옴. false면 불러온 데이터로 페이지 불러옴
			boolean checkerFirst = true;

			Cursor cursor = findNameSaveHandler.select();

			if (cursor.moveToFirst() && callBeforeTabs) {
				Log.d(TAG, "select count:" + cursor.getString(1) + "," + cursor.getString(2) + cursor.getString(0));
				String savedURLs = cursor.getString(1);
				String savedMyName = cursor.getString(2);
				// if (savedURLs == "null") {
				// clickMove.move(savedMyName);
				// } else {
				// clickMove.move(savedMyName);// 자식은 자식으로 들어가게 수정
				// }
				clickMove.directMove(savedURLs, savedMyName);
				checkerFirst = false;

			}
			if (cursor.isLast()) {
			}
			while (cursor.moveToNext() && callBeforeTabs) {
				String savedURLs = cursor.getString(1);
				String savedMyName = cursor.getString(2);
				// clickMove.move(savedMyName);
				clickMove.directMove(savedURLs, savedMyName);
				checkerFirst = false;

			}
			if (checkerFirst && MODE.equals(MODE_MIRROR)) {
				clickMove.move("FrontPage");
			} else if (checkerFirst && MODE.equals(MODE_WIKIPIDIA)) {
				clickMove.move("위키백과:대문");
			}
		}// else close
		findNameSaveHandler.removeAll();
		etMain.setImeOptions(EditorInfo.IME_ACTION_GO);
		etMain.setInputType(InputType.TYPE_CLASS_TEXT);

		etMain.setOnKeyListener(this);

		// 화면 밝기 기본 세팅
		// bright = (int) (mySharedPref.getBright() * 100);
		// WindowManager.LayoutParams params = getWindow().getAttributes();
		// params.screenBrightness = (float) bright / 100;
		// getWindow().setAttributes(params);
	}

	public HorizontalScrollView getHoriTab() {
		return horiTab;
	}

	public void setHoriTab(HorizontalScrollView horiTab) {
		this.horiTab = horiTab;
	}

	public boolean onKeyDown(int KeyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			if (KeyCode == KeyEvent.KEYCODE_BACK) {
				// 뒤로가기 버튼 제어
				LayoutInflater inflater = LayoutInflater.from(mContext);
				final LinearLayout view = (LinearLayout) inflater.inflate(R.layout.backbtn_click_dlg, null);

				final Button btnDlgCloseApp = (Button) view.findViewById(R.id.buttonDlgCloseApp);
				final Button btnDlgCloseCurrentTab = (Button) view.findViewById(R.id.buttonDlgCloseTab);
				final Button btnDlgCancle = (Button) view.findViewById(R.id.buttonDlgCancle);
				btnDlgCancle.setOnClickListener(this);
				btnDlgCloseApp.setOnClickListener(this);
				btnDlgCloseCurrentTab.setOnClickListener(this);
				mDlg = new AlertDialog.Builder(mContext).setView(view).show();
				return true;
			}

		}

		return super.onKeyDown(KeyCode, event);
	}

	public boolean isCallBeforeTabs() {
		return callBeforeTabs;
	}

	// public void setCallBeforeTabs(boolean callBeforeTabs) {
	// this.callBeforeTabs = callBeforeTabs;
	// }

	@Override
	public boolean onKey(View arg0, int keyCode, KeyEvent keyEvent) {

		if (keyCode == KeyEvent.KEYCODE_ENTER) {
			clickMove.move(getCurSelDoc());
			return true;
		}

		return false;
	}

	public EnhaDocument getCurSelDoc() {
		return curSelDoc;
	}

	public void setCurSelDoc(EnhaDocument curSelDoc) {
		this.curSelDoc = curSelDoc;
	}

	// 메뉴
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.main, menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	// 밝기 조절, 폰트 조절, 기타 설정, 전체 탭 닫기
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle item selection
		LayoutInflater adbInflater = LayoutInflater.from(mContext);
		View view;

		switch (item.getItemId()) {
		// 기타 세팅(서버 전송, 이전 탭 기억)
		case R.id.action_setting:
			// ExSettingsDlg dlg = new ExSettingsDlg(mContext);
			SettingDlg dlg = new SettingDlg(mContext);
			return true;
		case R.id.action_test:
			InDocFinderDlg finderDoc = new InDocFinderDlg(mContext);
			return true;

		case R.id.action_close_all_tab:
			findNameSaveHandler.removeAll();
			refresh.removeAll();
			for (int i = 0; i < getTopDocumentList().size(); i++) {
				Log.d(TAG, "delete it top Doc");
				getTopDocumentList().remove(i);
			}

			return true;

		case R.id.action_send_document:
			Intent sendIntent = new Intent();
			sendIntent.setAction(Intent.ACTION_SEND);
			sendIntent.putExtra(Intent.EXTRA_TEXT, getCurSelDoc().getCurTabName() + "에 관한 엔하문서입니다.\n" + getCurSelDoc().getUrl() + "\n" + "더 쉽고 편하게 엔하를 보시려면 \"엔하이지뷰어\"가 있습니다." + "\n"
						+ "https://play.google.com/store/apps/details?id=appulele.enhaviewer\n");
			sendIntent.setType("text/plain");
			startActivity(Intent.createChooser(sendIntent, "test"));

			break;
		default:
			return super.onOptionsItemSelected(item);
		}
		return true;
	}

	@Override
	public boolean onLongClick(View v) {
		WebView wv = (WebView) v;
		final WebView.HitTestResult result = wv.getHitTestResult();

		if (result != null) {
			switch (result.getType()) {
			case WebView.HitTestResult.IMAGE_TYPE:
			case WebView.HitTestResult.IMAGE_ANCHOR_TYPE:
			case WebView.HitTestResult.SRC_IMAGE_ANCHOR_TYPE:
				// case WebView.HitTestResult.:

				LayoutInflater inflater = LayoutInflater.from(this);

				final ImageView imageView = (ImageView) inflater.inflate(R.layout.imageviewer, null);
				// webViewer.loadData(downloadImg(result.getExtra()),
				// "text/html; charset=UTF-8", null);

				final AQuery aq = new AQuery(imageView);
				aq.id(R.id.ivDialog).image(result.getExtra(), false, true);

				// webViewer.loadData(downloadImg(result.getExtra()),
				// "text/html; charset=UTF-8", null);
				Log.i(TAG, "IMAGE_TYPE" + String.valueOf(result.getType()) + result.getExtra());
				new AlertDialog.Builder(mContext).setMessage("이미지를 다운로드 하기(BETA)").setView(imageView).setPositiveButton("저장", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int whichButton) {
						String dirPath = C.PATH_EXTERNAL + File.separator + "EnhaViewer" + File.separator;
						Random random = new Random();

						String filePath = dirPath + "EnhaViewer" + random.nextInt();
						// String filePath = dirPath + "EnhaViewer_" +
						// result.getExtra();
						Log.d(TAG, "filePathA" + filePath);
						DownImageTask task = new DownImageTask(aq, dirPath, filePath, result.getExtra(), mContext);
						task.execute();
					}
				}).setNegativeButton("닫기", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
					}
				}).show();

				// WebView wvs = new WebView(mContext);
				// wvs.
				// wvs.loadUrl(html);
				break;
			default:
				break;
			}
		}

		return false;
	}

	public ArrayList<EnhaDocument> getTopDocumentList() {
		return topDocumentList;
	}

	public void setTopDocumentList(ArrayList<EnhaDocument> topDocumentList) {
		this.topDocumentList = topDocumentList;
	}

	public void setDefaultPageColor(boolean bMode) {
		if (bMode)
			frame.setBackgroundColor(Color.BLACK);
		else
			frame.setBackgroundColor(Color.WHITE);
	}

	public void wrapFinder(String text) {
		WebView wv = getCurSelDoc().getWebView();
		@SuppressWarnings("deprecation")
		int i = wv.findAll(text); // is not supposed to crash//낮은 api16이하

		wv.findAllAsync(text);// is not supposed to crash//높은 api 16이상

		Toast.makeText(this, "총 " + i + "개의 \"" + text + "\" 을(를) 찾았습니다.", Toast.LENGTH_SHORT).show();
		try {
			Method m = WebView.class.getMethod("setFindIsUp", Boolean.TYPE);
			m.invoke(wv, true);
		} catch (Throwable ignored) {

		}

	}

}
