package appulele.enhaviewer;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

public class NetworkCheck {
	public static String TAG = "NetworkCheck";

	// 네트워크 상태 체크하기
	public static boolean networkCheck(Context mContext) {
		boolean isConnect = false;
		try {
			ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
			NetworkInfo mobile = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

			if (wifi.isConnected() || mobile.isConnected()) {
				// WIFI, 3G 어느곳에도 연결되지 않았을때
				isConnect = true;
				Log.d(TAG, "Network connect success");
			} else {
				isConnect = false;
				Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return isConnect;
	}

}
