package appulele.enhaviewer;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Toast;
import android.widget.ViewFlipper;
import appulele.enhaviewer.R;

/**
 * 탭을 새로고침해주는 함수 모음
 * @author sanghyeon
 *
 */
public class Refresh implements OnClickListener {
	boolean D = true;
	String TAG = getClass().getSimpleName().toString();
	Context mContext;
	LinearLayout llTabMid;
	LinearLayout llTabNavi;
	ArrayList<EnhaDocument> topDocumentList;
	ViewFlipper viewFlipper;
	MainActivity mainActivity;
	HorizontalScrollView horiTab;

	public Refresh(Context context) {
		// TODO Auto-generated constructor stub
		mContext = context;
		mainActivity = (MainActivity) mContext;
		llTabMid = mainActivity.llTabMid;
		llTabNavi = mainActivity.llTabNavi;
		topDocumentList = mainActivity.getTopDocumentList();
		viewFlipper = mainActivity.viewFlipper;
		horiTab = mainActivity.horiTab;
	}

	/**
	 * 탭을 클릭시에 뷰 리프레쉬
	 * 
	 * @param curSelDoc
	 */
	@SuppressLint("NewApi")
	public void refreshTabLayout(EnhaDocument curSelDoc) {
		llTabMid.removeAllViews();
		llTabNavi.removeAllViews();
		final int position = mainActivity.getDocPosition(curSelDoc);
		for (EnhaDocument tabChild : topDocumentList) {
			// 각 최상위 탭마다, 자기 자신과 자신의 자식들이 레이아웃에 추가되는 함수
			ArrayList<EnhaDocument> list = tabChild.getDocList();
			for (EnhaDocument doc : list) {
				llTabMid.addView(doc.getTab());
			}
			viewFlipper.setDisplayedChild(position);
		}

		for (int i = 0; i < llTabMid.getChildCount(); i++) {
			EnhaDocument childDoc = (EnhaDocument) llTabMid.getChildAt(i).getTag();

			LinearLayout tab = (LinearLayout) llTabMid.getChildAt(i);
			LayoutParams params = new LayoutParams(210, 50);
//			params.setMargins(0, 30, 0, 0);
//			tab.setLayoutParams(params);
			if (childDoc == curSelDoc) {
				tab.setBackgroundResource(R.drawable.selected_tab);
			} else {
				tab.setBackgroundResource(R.drawable.unselected_tab);
			}
			
			LayoutInflater inflater = (LayoutInflater) mainActivity.getSystemService( Context.LAYOUT_INFLATER_SERVICE ); 
			LinearLayout layoutSide  = null;
			layoutSide = (LinearLayout) inflater.inflate( R.layout.sidetab,layoutSide);
			ImageButton closeSideTab  =(ImageButton) tab.findViewById(R.id.ibTabClose); 
			Button btnGet =  (Button)layoutSide.findViewById(R.id.buttonSideText);
//			btnGet.setBackgroundColor(Color.WHITE);
			btnGet.setText(((Button) tab.getChildAt(0)).getText());
			btnGet.setTag(childDoc);
			btnGet.setOnClickListener(mainActivity);
//			params = new LayoutParams(240, 60);
			 
//			btnGet.setLayoutParams(params);

//			btnGet.setBackgroundResource(R.drawable.round_box);

//			LinearLayout layoutSide = new LinearLayout(mContext);
//			params = new LayoutParams(300, 70);
//			params.setMargins(20,10,  0, 10);
//			layoutSide.setLayoutParams(params);
//			layoutSide.setOrientation(LinearLayout.HORIZONTAL);
//			layoutSide.setBackgroundResource(R.drawable.round_box);
//			layoutSide.addView(btnGet);

//			Button closeSideTab = new Button(mContext);
			closeSideTab.setTag(childDoc);
//			closeSideTab.setBackgroundResource(R.drawable.x_btn2);
			closeSideTab.setOnClickListener(this);
//			closeSideTab.setPadding(0, 0, 0, 40);
//			closeSideTab.setLayoutParams(new LayoutParams(30, 30));
//			layoutSide.addView(closeSideTab);
			llTabNavi.addView(layoutSide);
		}

		Handler handlerTimer = new Handler();
		handlerTimer.postDelayed(new Runnable() {
			public void run() {
				viewFlipper.setDisplayedChild(position);
				int positionVFlipper = viewFlipper.getDisplayedChild();
				horiTab.scrollTo(220 * (positionVFlipper + 1), 0);
			}
		}, 50);

	}

	@SuppressLint("NewApi")
	public void refreshTabLayout(int position) {
		refreshTabLayout();
	 
	}

	/**
	 * 화면 갱신
	 * 
	 * @param position
	 */
	@SuppressLint("NewApi")
	public void refreshTabLayout() {
		llTabMid.removeAllViews();
		llTabNavi.removeAllViews();

		for (final EnhaDocument tabChild : topDocumentList) {
			// 각 최상위 탭마다, 자기 자신과 자신의 자식들이 레이아웃에 추가되는 함수
			ArrayList<EnhaDocument> list = tabChild.getDocList();
			for (EnhaDocument doc : list) {
				llTabMid.addView(doc.getTab());
			}
		}
		for (int i = 0; i < llTabMid.getChildCount(); i++) {
			EnhaDocument childDoc = (EnhaDocument) llTabMid.getChildAt(i).getTag();

			Button btnGet = new Button(mContext);
			btnGet.setBackgroundResource(R.drawable.round_box);
			btnGet.setBackgroundColor(Color.WHITE);
			LinearLayout tab = (LinearLayout) llTabMid.getChildAt(i);
//			LayoutParams params = new LayoutParams(210, 50);
//			params.setMargins(0, 30, 0, 0);
//			tab.setLayoutParams(params);
			if (childDoc == mainActivity.getCurSelDoc()) {
				tab.setBackgroundResource(R.drawable.selected_tab);
			} else {
				tab.setBackgroundResource(R.drawable.unselected_tab);
			}

			btnGet.setText(((Button) tab.getChildAt(0)).getText());
			btnGet.setTag(childDoc);
			LayoutParams params = new LayoutParams(240, 60);
//			 
//			btnGet.setLayoutParams(params);
			btnGet.setOnClickListener(mainActivity);
			btnGet.setBackgroundResource(R.drawable.round_box);
			LinearLayout layoutSide = new LinearLayout(mContext);
			params = new LayoutParams(300, 70);
			params.setMargins(20,10,  0, 10);
			layoutSide.setLayoutParams(params);
			layoutSide.setOrientation(LinearLayout.HORIZONTAL);
			layoutSide.setBackgroundResource(R.drawable.round_box);
			layoutSide.addView(btnGet);

			Button closeSideTab = new Button(mContext);
			closeSideTab.setBackgroundResource(R.drawable.x_btn2);
			closeSideTab.setTag(childDoc);
			closeSideTab.setPadding(0, 0, 0, 40);
			closeSideTab.setLayoutParams(new LayoutParams(30, 30));
			closeSideTab.setOnClickListener(this);
			layoutSide.addView(closeSideTab);
			llTabNavi.addView(layoutSide);

		}

	}

	/**
	 * viewFlipper와 tabList의 뷰를 삭제한다.
	 */
	public void removeAll() {
		topDocumentList.removeAll(topDocumentList);
		llTabMid.removeAllViews();
		llTabNavi.removeAllViews();
		viewFlipper.removeAllViews();
	}

	@Override
	public void onClick(View v) {
		Toast.makeText(mContext, "delete", Toast.LENGTH_SHORT).show();
		EnhaDocument doc = (EnhaDocument) v.getTag();
		mainActivity.deleteDoc(doc);

	}
}
