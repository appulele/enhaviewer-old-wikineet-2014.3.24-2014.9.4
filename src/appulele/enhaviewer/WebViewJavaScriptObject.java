package appulele.enhaviewer;

import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.sax.StartElementListener;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast; 

/**
 * HTML에서 자바 스크립트 동작 하는 함수 구현
 * 
 * @author sanghyeon
 * 
 */
public class WebViewJavaScriptObject {
	private final static String TAG = "webViewJavaScruptObject";

	private final static boolean D = false;

	/**
	 * 현재 선택된 DOC가 있으면 그 DOC를 부모로 한다. 만약 부모가 없으면 없는 동작
	 * 
	 * @param mContext
	 * @param curSelectedDoc
	 * @return
	 */
	public static Object getObject(final Context mContext, final EnhaDocument curSelectedDoc) {
		return new Object() {

			// HTML에서 링크 클릭시 새 탭 추가
			@JavascriptInterface
			public void createLinkTab(String text) {
				if (D)
					Toast.makeText(mContext, "create" + text, Toast.LENGTH_SHORT).show();

			}

			// 외부 이미지, iframe(유투브)
			@JavascriptInterface
			public void external(String url) {
				// 해당 url이미지 팝업으로 띄워주기
				if (D)
					Toast.makeText(mContext, "img url:" + url, Toast.LENGTH_SHORT).show();
				// url의 문자열에서 youtube를 발견하면 동영상으로(아직 미구현), 아니면 이미지를 보여준다.
				ExternalNewDialog.newDialog(mContext, url);
			}

			// 외부 링크
			@JavascriptInterface
			public void externalLink(String url) {
				// 해당 url웹뷰 팝업으로 띄워주기
				if (D)
					Toast.makeText(mContext, "외부 링크", Toast.LENGTH_SHORT).show();
				ExternalLinkNewDialog.newDialog(mContext, url);
			}

			// 주석
			@JavascriptInterface
			public void foot(String text) {
				if (D)
					Log.d(TAG, "주석: " + text);
				// 해당 주석 팝업 띄워주기
				FootNewDialog.newDialog(mContext, text);
			}

			//동영상 링크 
			@JavascriptInterface
			public void movie(String text) {
				if (D)
					Log.d(TAG, "동영상 : " + text);
				// 해당 주석 팝업 띄워주기
				Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(text)); 
				mContext.startActivity(intent);
			}

			/**
			 * 검색 다음 페이지 이동 webview 를 재사용하기 어렵다.
			 * 
			 * @param text
			 */
			@JavascriptInterface
			public void page(String text) {
				// Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
				// ConvertHtml convert = new ConvertHtml(wv, mContext);
				// AddDocTask addDocTask = new AddDocTask(curSelectedDoc,
				// "http://mirror.enha.kr/wiki/" + encodedKeyword, str,
				// mContext);
				// addDocTask.execute();

				boolean networkConnect = NetworkCheck.networkCheck(mContext);
				if (networkConnect) {
					// fix2
					// AddDocTask addDocTask = new AddDocTask(curSelectedDoc,
					// "http://mirror.enha.kr/search/" + text,"", mContext);
					// Log.d(TAG, "checker "+ "http://mirror.enha.kr/search/" +
					// encodedKeyword);
					// addDocTask.execute();
					String URLSearch = URLs.URLEnhaSearch;
					ConvertHtml convert = new ConvertHtml(mContext, URLSearch + text);
					convert.execute();
				} else {
					Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
				}

			}

			// 위키 페이지 이동
			@JavascriptInterface
			public void wiki(String str) {
				// 해당 위키 페이지로 이동
				final String MODE_MIRROR = "MIRROR";
				final String MODE_WIKIPIDIA = "WIKIPIDIA";

				if (D)
					Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
				if (D)
					Log.d(TAG, str);
				boolean networkConnect = NetworkCheck.networkCheck(mContext);
				if (networkConnect) {
					String encodedKeyword = "";
					try {
						encodedKeyword = URLEncoder.encode(str, "utf-8");
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					}
					// fix2
					String urlWiki = MainActivity.MODE;
					if (urlWiki.equals(MODE_MIRROR)) {
						urlWiki = URLs.URLEnhaWiki;
					} else if (urlWiki.equals(MODE_WIKIPIDIA)) {
						urlWiki = URLs.URLPidiaWiki;
					}

					AddDocTask addDocTask = new AddDocTask(curSelectedDoc, urlWiki + encodedKeyword, str, mContext);
					addDocTask.execute();
					if (D)
						Log.d(TAG, "hasParent webJS");
				} else {
					Toast.makeText(mContext, "네트워크 연결이 원활하지 않습니다.", Toast.LENGTH_LONG).show();
				}
			}

			// 그냥 바로 링크 default 브라우저로 띄우기
			@JavascriptInterface
			public void nextList(final String url) {
				if (D)
					Toast.makeText(mContext, "OTHERS", Toast.LENGTH_SHORT).show();

			}

			// 그냥 바로 링크 default 브라우저로 띄우기
			@JavascriptInterface
			public void others(String url) {
				// urlWiki가 http로 시작하면 그대로 아니면 http 붙여서
				if (!url.startsWith("http") && url.startsWith("//")) {
					url = "http:" + url;
				}
				this.externalLink(url);

				if (D)
					Toast.makeText(mContext, "OTHERS", Toast.LENGTH_SHORT).show();

			}
		};
	}

}
