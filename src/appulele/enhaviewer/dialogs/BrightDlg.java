package appulele.enhaviewer.dialogs;

import net.daum.adam.publisher.impl.container.k.m;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import appulele.enhaviewer.MainActivity;
import appulele.enhaviewer.R;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;
/**
 * 구버전, SettingDlg로 통합 됨
 * @author sanghyeon
 *
 */
public class BrightDlg {
	MainActivity mActivity;
	private int bright = 60;
	 
	
	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;
	public BrightDlg(Context mContext) {
		mActivity = (MainActivity)mContext;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		//inits
 
		LayoutInflater adbInflater = LayoutInflater.from(mContext);
		View view = adbInflater.inflate(R.layout.setting, null);
		adbInflater = LayoutInflater.from(mContext); 
		view = adbInflater.inflate(R.layout.brightcontrol, null);

		final SeekBar sbBright = (SeekBar) view.findViewById(R.id.seekBarBright);
		final TextView tvBrightNow = (TextView) view.findViewById(R.id.tvBrightNow);
		// seekbar 텍스트 크기 조절
		WindowManager.LayoutParams params = mActivity.getWindow().getAttributes();

		// tvBrightNow.setText(bright);
		tvBrightNow.setText("" + 50);
		sbBright.setProgress(50);
		sbBright.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				if (progress < 3) {
					bright = progress + 3;
				} else
					bright = progress;

				sbBright.setProgress(progress);
				tvBrightNow.setText("" + progress);
			}
		});

		new AlertDialog.Builder(mContext).setView(view).setMessage("설정").setPositiveButton("확인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// 어플 밝기 조절

				WindowManager.LayoutParams params = mActivity.getWindow().getAttributes();
				params.screenBrightness = (float) bright / 100;
				tvBrightNow.setText("" + bright);
				mActivity.getWindow().setAttributes(params);
				// mySharedPref.setBright(bright);
			}
		}).show();

	}
}
