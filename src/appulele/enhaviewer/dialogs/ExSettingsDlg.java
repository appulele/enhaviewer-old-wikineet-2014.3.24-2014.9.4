package appulele.enhaviewer.dialogs;

import android.R.color;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.Toast;
import appulele.enhaviewer.EnhaDocument;
import appulele.enhaviewer.MainActivity;
import appulele.enhaviewer.R;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;
/**
 * 구버전, SettingDlg로 통합 됨
 * @author sanghyeon
 *
 */
public class ExSettingsDlg {
	MainActivity mActivity;

	private boolean sendFindNames = true;

	private boolean callBeforeTabs = true;
	private boolean blackMode = false;
	private boolean newFootDlg = true;
	private boolean createMovieButton = false;
	private boolean directMove= true;

	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;

	public ExSettingsDlg(Context mContext) {
		mActivity = (MainActivity) mContext;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		// inits
		callBeforeTabs = mySharedPref.isCallSavedTabs();
		blackMode = mySharedPref.isBlackMode();
		newFootDlg = mySharedPref.isNewFootDlg();
		createMovieButton = mySharedPref.isCreateMovieBtn();
		sendFindNames = mySharedPref.isSendFindNames();
		directMove = mySharedPref.isDirectMove();
		
		
		
		LayoutInflater adbInflater = LayoutInflater.from(mContext);
		View view = adbInflater.inflate(R.layout.setting, null);
		final CheckBox cbCallSavedTabs = (CheckBox) view.findViewById(R.id.cbCallBeforeTabs);
		cbCallSavedTabs.setChecked(callBeforeTabs);
		final CheckBox cbCreateMovieBtn = (CheckBox) view.findViewById(R.id.cbCreateMovieButton);
		cbCreateMovieBtn.setChecked(createMovieButton);
		final CheckBox cbBlackMode = (CheckBox) view.findViewById(R.id.cbCreateBlackMode);
		cbBlackMode.setChecked(blackMode);
		final CheckBox cbNewFootDlg = (CheckBox) view.findViewById(R.id.cbCreateNewDialog);
		cbNewFootDlg.setChecked(newFootDlg);
		final CheckBox cbDirectMove = (CheckBox) view.findViewById(R.id.cbCreateDirectMove);
		cbDirectMove.setChecked(directMove);
		
		final CheckBox cbSendFindNames = (CheckBox) view.findViewById(R.id.cbSendFindNames);
		cbSendFindNames.setChecked(sendFindNames);

		new AlertDialog.Builder(mContext).setView(view).setMessage("설정").setPositiveButton("확인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				cbCallSavedTabs.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbSendFindNames.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					}
				});
				cbCreateMovieBtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbBlackMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbNewFootDlg.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbDirectMove.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});

				callBeforeTabs = cbCallSavedTabs.isChecked();
				createMovieButton = cbCreateMovieBtn.isChecked();
				sendFindNames = cbSendFindNames.isChecked();
				blackMode = cbBlackMode.isChecked();
				newFootDlg = cbNewFootDlg.isChecked();
				directMove = cbDirectMove.isChecked();
				
				mySharedPref.setCreateMovieBtn(createMovieButton);
				mySharedPref.setSendFindNames(sendFindNames);
				mySharedPref.setCallSavedTabs(callBeforeTabs);
				mySharedPref.setBlackMode(blackMode);
				mySharedPref.setNewFootDlg(newFootDlg);
				mySharedPref.setDirectMove(directMove); 
				
				// 기본 페이지 백색/흑색
				mActivity.setDefaultPageColor(blackMode);

				// 모든 탭 사이즈 바꾸기
				for (int i = 0; i < mActivity.getTopDocumentList().size(); i++) {
					EnhaDocument docs = mActivity.getTopDocumentList().get(i);
					for (int j = 0; j < docs.getDocList().size(); j++) {
						EnhaDocument doc = docs.getDocList().get(j);
						if(blackMode)doc.getWebView().setBackgroundColor(color.black); 
						else doc.getWebView().setBackgroundColor(color.white);
					}
				}
			}
		}).show();

	}
}
