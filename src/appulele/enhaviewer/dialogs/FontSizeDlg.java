package appulele.enhaviewer.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import appulele.enhaviewer.EnhaDocument;
import appulele.enhaviewer.MainActivity;
import appulele.enhaviewer.R;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;
/**
 * 구버전, SettingDlg로 통합 됨
 * @author sanghyeon
 *
 */
public class FontSizeDlg {
	MainActivity mActivity;
	 
	private boolean callBeforeTabs = true;    
	private int fontSize = 13; 
	 
	
	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;
	public FontSizeDlg(Context mContext) {
		mActivity = (MainActivity)mContext;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		//inits
		 fontSize = mySharedPref.getFontSize();
		 callBeforeTabs = mySharedPref.isCallSavedTabs();
		LayoutInflater adbInflater = LayoutInflater.from(mContext);
		View view = adbInflater.inflate(R.layout.setting, null);
		adbInflater = LayoutInflater.from(mContext);
		view = adbInflater.inflate(R.layout.fontsize, null);

		final EditText etFontSize = (EditText) view.findViewById(R.id.editTextFontSize);
		final TextView tvNow = (TextView) view.findViewById(R.id.textviewNow);
		final SeekBar sbTextSize = (SeekBar) view.findViewById(R.id.seekBarTextSize);
		sbTextSize.setProgress(fontSize);
		etFontSize.setText("" + fontSize);
		// seekbar 텍스트 크기 조절
		sbTextSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				// if (progress < 10) {
				// progress = 10;
				sbTextSize.setProgress(progress);
				// etFontSize.setText("" + progress);
				// }
				WindowManager.LayoutParams params =mActivity.getWindow().getAttributes();
				etFontSize.setText("" + (progress + 10));
			}
		});

		tvNow.setText("  현재 Size:[" + fontSize + "]");

		new AlertDialog.Builder(mContext).setView(view).setMessage("설정").setPositiveButton("확인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				fontSize = Integer.parseInt(etFontSize.getText().toString());
				mySharedPref.setFontSize(Integer.parseInt(etFontSize.getText().toString()));
 
				// 모든 탭 사이즈 바꾸기
				for (int i = 0; i <mActivity.getTopDocumentList().size(); i++) {
					EnhaDocument docs = mActivity.getTopDocumentList().get(i);
					for (int j = 0; j < docs.getDocList().size(); j++) {
						EnhaDocument doc = docs.getDocList().get(j);
						WebSettings wvSettings = doc.getWebView().getSettings();
						wvSettings.setLoadWithOverviewMode(true);
						wvSettings.setUseWideViewPort(true);
						wvSettings.setDefaultFontSize(fontSize);
						wvSettings.setDefaultFixedFontSize(fontSize);
						wvSettings.setMinimumFontSize(fontSize - 2);
					}

				}
			}
		}).show();
		
		
		
	 
	}
}
