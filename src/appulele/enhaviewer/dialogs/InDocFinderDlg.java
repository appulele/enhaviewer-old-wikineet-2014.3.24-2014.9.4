package appulele.enhaviewer.dialogs;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import appulele.enhaviewer.MainActivity;
import appulele.enhaviewer.R;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;

public class InDocFinderDlg {
	MainActivity mActivity;

	private boolean sendFindNames = true;

	private boolean callBeforeTabs = true;
	private boolean blackMode = false;
	private boolean newFootDlg = true;
	private boolean createMovieButton = false;
	private boolean directMove= true;

	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;

	public InDocFinderDlg(Context mContext) {
		mActivity = (MainActivity) mContext;
	 
		LayoutInflater adbInflater = LayoutInflater.from(mContext);
		View view = adbInflater.inflate(R.layout.textview_dlg, null);
		final EditText etFinder = (EditText)view.findViewById(R.id.etInDocFinder);
	 
		 
//		tvFinder.setOnClickListener(mActivity);
		new AlertDialog.Builder(mContext).setView(view) .setPositiveButton("확인", new DialogInterface.OnClickListener() { 
			public void onClick(DialogInterface dialog, int whichButton) {

			mActivity.wrapFinder( etFinder.getText().toString());
			}
		}).show();

	}
}
