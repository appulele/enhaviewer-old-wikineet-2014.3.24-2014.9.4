package appulele.enhaviewer.dialogs;

import android.R.color;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import appulele.enhaviewer.EnhaDocument;
import appulele.enhaviewer.MainActivity;
import appulele.enhaviewer.R;
import appulele.enhaviewer.sharedprefrence.mySharedPrefrence;

public class SettingDlg {
	MainActivity mActivity;

	private boolean sendFindNames = true;

	private boolean callBeforeTabs = true;
	private boolean blackMode = false;
	private boolean newFootDlg = true;
	private boolean createMovieButton = false;
	private boolean directMove= true;
	private int bright = 60;
	private int fontSize = 13; 
	private int sensitivity = 210;
	mySharedPrefrence mySharedPref;
	SharedPreferences sharedPref;

	public SettingDlg(Context mContext) {
		mActivity = (MainActivity) mContext;
		sharedPref = PreferenceManager.getDefaultSharedPreferences(mActivity);
		mySharedPref = new mySharedPrefrence(mContext, sharedPref);
		// inits
		callBeforeTabs = mySharedPref.isCallSavedTabs();
		blackMode = mySharedPref.isBlackMode();
		newFootDlg = mySharedPref.isNewFootDlg();
		createMovieButton = mySharedPref.isCreateMovieBtn();
		sendFindNames = mySharedPref.isSendFindNames();
		directMove = mySharedPref.isDirectMove();
		 fontSize = mySharedPref.getFontSize();
//			Toast.makeText(mActivity, "3size:"+fontSize	,Toast.LENGTH_SHORT).show();
		 sensitivity = mySharedPref.getSensitivity();
		 
		 
		LayoutInflater adbInflater = LayoutInflater.from(mContext);
		View view = adbInflater.inflate(R.layout.setting_a, null);
		final CheckBox cbCallSavedTabs = (CheckBox) view.findViewById(R.id.cbCallBeforeTabs);
		cbCallSavedTabs.setChecked(callBeforeTabs);
		final CheckBox cbCreateMovieBtn = (CheckBox) view.findViewById(R.id.cbCreateMovieButton);
		cbCreateMovieBtn.setChecked(createMovieButton);
		final CheckBox cbBlackMode = (CheckBox) view.findViewById(R.id.cbCreateBlackMode);
		cbBlackMode.setChecked(blackMode);
		final CheckBox cbNewFootDlg = (CheckBox) view.findViewById(R.id.cbCreateNewDialog);
		cbNewFootDlg.setChecked(newFootDlg);
		final CheckBox cbDirectMove = (CheckBox) view.findViewById(R.id.cbCreateDirectMove);
		cbDirectMove.setChecked(directMove); 
		final CheckBox cbSendFindNames = (CheckBox) view.findViewById(R.id.cbSendFindNames);
		cbSendFindNames.setChecked(sendFindNames); 
		final SeekBar sbBright = (SeekBar) view.findViewById(R.id.seekBarBright);
		final TextView tvBrightNow = (TextView) view.findViewById(R.id.tvBrightNow);
		// seekbar 텍스트 크기 조절
		WindowManager.LayoutParams params = mActivity.getWindow().getAttributes(); 
		// tvBrightNow.setText(bright);
		tvBrightNow.setText("" + 50);
		sbBright.setProgress(50);
		sbBright.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				if (progress < 3) {
					bright = progress + 3;
				} else
					bright = progress;

				sbBright.setProgress(progress);
				tvBrightNow.setText("" + progress);
			}
		});
		
		final EditText etFontSize = (EditText) view.findViewById(R.id.editTextFontSize);
		final TextView tvNow = (TextView) view.findViewById(R.id.textviewNow);
		final SeekBar sbTextSize = (SeekBar) view.findViewById(R.id.seekBarTextSize);
		sbTextSize.setProgress(fontSize);
		etFontSize.setText("" + fontSize);
		// seekbar 텍스트 크기 조절
		sbTextSize.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				// if (progress < 10) {
				// progress = 10;
				sbTextSize.setProgress(progress);
				// etFontSize.setText("" + progress);
				// }
				WindowManager.LayoutParams params =mActivity.getWindow().getAttributes();
				etFontSize.setText("" + (progress + 10));
			}
		});

		tvNow.setText("  현재 Size:[" + fontSize + "]");

		final EditText etSensor = (EditText) view.findViewById(R.id.editTextSensitivity);
		final TextView tvSensorNow = (TextView) view.findViewById(R.id.textviewSensitiivtyNow);
		final SeekBar sbSensor = (SeekBar) view.findViewById(R.id.seekBarSensor);
		sbSensor.setProgress(sensitivity);
		etSensor.setText("" + sensitivity);
		// seekbar 텍스트 크기 조절
		sbSensor.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
			}

			@Override
			public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

				// if (progress < 10) {
				// progress = 10;
				sbSensor.setProgress(progress);
				// etFontSize.setText("" + progress);
				// }
				WindowManager.LayoutParams params =mActivity.getWindow().getAttributes();
				etSensor.setText("" + (progress + 10));
			}
		});

		tvSensorNow.setText("  현재 Size:[" + sensitivity + "]");

		new AlertDialog.Builder(mContext).setView(view).setMessage("설정").setPositiveButton("확인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				//********************************************체크박스 설정 부 *************************
				cbCallSavedTabs.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbSendFindNames.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					}
				});
				cbCreateMovieBtn.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbBlackMode.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbNewFootDlg.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});
				cbDirectMove.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

					}
				});

				callBeforeTabs = cbCallSavedTabs.isChecked();
				createMovieButton = cbCreateMovieBtn.isChecked();
				sendFindNames = cbSendFindNames.isChecked();
				blackMode = cbBlackMode.isChecked();
				newFootDlg = cbNewFootDlg.isChecked();
				directMove = cbDirectMove.isChecked();
				
				mySharedPref.setCreateMovieBtn(createMovieButton);
				mySharedPref.setSendFindNames(sendFindNames);
				mySharedPref.setCallSavedTabs(callBeforeTabs);
				mySharedPref.setBlackMode(blackMode);
				mySharedPref.setNewFootDlg(newFootDlg);
				mySharedPref.setDirectMove(directMove); 
				
				// 기본 페이지 백색/흑색
				mActivity.setDefaultPageColor(blackMode);

				// 모든 탭 사이즈 바꾸기
				for (int i = 0; i < mActivity.getTopDocumentList().size(); i++) {
					EnhaDocument docs = mActivity.getTopDocumentList().get(i);
					for (int j = 0; j < docs.getDocList().size(); j++) {
						EnhaDocument doc = docs.getDocList().get(j);
						if(blackMode)doc.getWebView().setBackgroundColor(color.black); 
						else doc.getWebView().setBackgroundColor(color.white);
					}
				}
				//*******************************체크박스 설정부 ********************************
				
				
				//*******************************폰트 사이즈 설정부 ******************************
				fontSize = Integer.parseInt(etFontSize.getText().toString());
				mySharedPref.setFontSize(fontSize);
//				Toast.makeText(mActivity, "1size:"+fontSize	,Toast.LENGTH_SHORT).show();
				// 모든 탭 사이즈 바꾸기
				for (int i = 0; i <mActivity.getTopDocumentList().size(); i++) {
					EnhaDocument docs = mActivity.getTopDocumentList().get(i);
					for (int j = 0; j < docs.getDocList().size(); j++) {
						EnhaDocument doc = docs.getDocList().get(j);
						WebSettings wvSettings = doc.getWebView().getSettings();
						wvSettings.setLoadWithOverviewMode(true);
						wvSettings.setUseWideViewPort(true);
						wvSettings.setDefaultFontSize(fontSize);
						wvSettings.setDefaultFixedFontSize(fontSize);
						wvSettings.setMinimumFontSize(fontSize - 2);
					}

				}
				//****************************폰트사이즈 설정부************************************
				
				
				//****************************밝기 설정부************************************
				WindowManager.LayoutParams params = mActivity.getWindow().getAttributes();
				params.screenBrightness = (float) bright / 100;
				tvBrightNow.setText("" + bright);
				mActivity.getWindow().setAttributes(params);
				//****************************밝기 설정부************************************
				
				
				//****************************감도설정부************************************
				sensitivity = Integer.parseInt(etSensor.getText().toString()); 
				mySharedPref.setSensitivity(sensitivity); 
				
				//****************************감도설정부************************************
			}
		}).show(); 

	
	}
}
