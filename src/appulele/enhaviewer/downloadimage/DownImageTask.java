package appulele.enhaviewer.downloadimage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import com.androidquery.AQuery;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import appulele.enhaviewer.MainActivity;

@SuppressLint("SetJavaScriptEnabled")
public class DownImageTask extends AsyncTask<Void, Void, Void> {
	/**
	 * 이미지를 다운 받는다.
	 * */
	private final String TAG = this.getClass().getSimpleName();
	String filePath;
	String dirPath;
	AQuery aq;
	String weburl;
	Context mContext;
	MainActivity activity;

	public DownImageTask(AQuery aq, String dirPath, String filePath, String url, Context mContext) {
		this.aq = aq;
		this.filePath = filePath + ".jpg";
		weburl = url;
		this.mContext = mContext;
		this.dirPath = dirPath;
		activity = (MainActivity) mContext;
	}

	@Override
	protected Void doInBackground(Void... params) {
		makeDirectory(dirPath);
		downloadBitmapImage();

		// copyFile(file , filePath);

		return null;

	}

	// Post Execute
	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);

		// Toast.makeText(mContext, R.string.down_img,
		// Toast.LENGTH_SHORT).show();
		File dir = new File(filePath);
		if (!dir.exists()) {
			dir.mkdirs();
			Log.i(TAG, "!dir.exists");
		} else {
			Log.d(TAG, "filePathB" + filePath);
//			mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + filePath)));
			mContext.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory() + dirPath)));
			Toast.makeText(mContext, "Saved...\nPath: " + filePath, Toast.LENGTH_LONG).show();
		}

	}

	/**
	 * 디렉토리 생성
	 * 
	 * @return dir
	 */
	private File makeDirectory(String dir_path) {
		File dir = new File(dir_path);
		if (!dir.exists()) {
			dir.mkdirs();
			Log.i(TAG, "!dir.exists");
		} else {
			Log.i(TAG, "dir.exists");
		}

		return dir;
	}

	void downloadBitmapImage() {
		InputStream input;
		File dir = new File(filePath);
		Log.d("tag", "start  file path:" + filePath);
		if (dir.exists()) {
			Log.d("tag", "start  file exist:" + filePath);
			return;
		}
		try {
			Log.d("tag", "try webUrl:" + weburl);
			URL url = new URL(weburl);
			input = url.openStream();
			byte[] buffer = new byte[1500];
			OutputStream output = new FileOutputStream(filePath);
			try {
				int bytesRead = 0;
				while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
					output.write(buffer, 0, bytesRead);
					Log.d("tag", "try2 webUrl:" + weburl);
				}
			} catch (Exception e) {
				Log.d("tag", "catch1 " + e.toString());
			} finally {
				output.close();
				buffer = null;
				Log.d("tag", "try3 webUrl:" + weburl);
				activity.sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED, Uri.parse("file://" + Environment.getExternalStorageDirectory())));
			}
		} catch (Exception e) {
			Log.d("tag", "catch2 " + e.toString());
		}

	}

	// Bitmap readBitmapImage()
	// {
	// String imageInSD = dirPath+();
	// BitmapFactory.Options bOptions = new BitmapFactory.Options();
	// bOptions.inTempStorage = new byte[16*1024];
	//
	// bitmap = BitmapFactory.decodeFile(imageInSD,bOptions);
	//
	// return bitmap;
	// }
}
