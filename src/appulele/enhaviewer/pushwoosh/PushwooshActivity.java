//package appulele.enhaviewer.pushwoosh;
//
//import org.mospi.moml.framework.pub.core.MOMLActivity;
//
//import com.applusform.pushwoosh.PushwooshObjectComponent;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//
//public class PushwooshActivity extends MOMLActivity {
//    
//	
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        
//        // for Pushwoosh 1/2 : applicationCode �� projectNumber �� �˸°� �����մϴ�.   
//        {
//	        String applicationCode = "BF1BB-C1181"; // http://pushwoosh.com �� ��ϵǾ� �ִ� ���� Application code
//	        String projectNumber = "901027535565"; // https://code.google.com/apis/console �� Overmenu �޴��� Dashboard�� ǥ�õǴ� Project Number
//	        PushwooshObjectComponent.setAppInfo(applicationCode, projectNumber);
//	        
//	        getMomlView().registerObjectComponent("com.applusform.pushwoosh.PushwooshObjectComponent", "pushwoosh", "object", this);
//        }
//        
//        this.loadApplication("moml/applicationInfo.xml");
//    }
//    
//    protected void onDestroy() {
//    	super.onDestroy();
//    	System.exit(0);
//    }
//    
//    // for Pushwoosh 2/2 : Pushwoosh Object�� onResume, onPause, onNewIntent �Լ��� ȣ���մϴ�.    
//	@Override
//	public void onResume() {
//		super.onResume();
//		PushwooshObjectComponent object = PushwooshObjectComponent.getObject(getMomlView());
//		object.onResume();
//	}
//
//	@Override
//	public void onPause() {
//		super.onPause();
//		PushwooshObjectComponent object = PushwooshObjectComponent.getObject(getMomlView());
//		object.onPause();
//	}
//
//	@Override
//	protected void onNewIntent(Intent intent) {
//		super.onNewIntent(intent);
//		PushwooshObjectComponent object = PushwooshObjectComponent.getObject(getMomlView());
//		object.onNewIntent(intent);
//	}
//}