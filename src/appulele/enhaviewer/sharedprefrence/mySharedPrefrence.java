package appulele.enhaviewer.sharedprefrence;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.hardware.Sensor;
import android.util.Log;
import android.view.View;
import android.webkit.WebView.FindListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import appulele.enhaviewer.MainActivity;
import appulele.enhaviewer.R;

public class mySharedPrefrence {
	private final boolean D = false;
	String TAG = getClass().getSimpleName();
	Context mContext;
	MainActivity mainActivity;
	private SharedPreferences sharedPref;
	private boolean callSavedTabs = true;
	private int fontSize = 13;
	private int sensitivity = 210;
	private boolean sendFindNames = true;
	private float bright = 0.5f;
	private boolean createMovieBtn = false; 
	private boolean blackMode = false;
	private boolean newFootDlg = true;
	private boolean directMove = true;
	
	public mySharedPrefrence(Context context, SharedPreferences sharedPref) {
		mContext = context;
		mainActivity = (MainActivity) context;
		this.sharedPref = sharedPref;
		callSavedTabs = sharedPref.getBoolean("callSavedTabs", true);
		sendFindNames = sharedPref.getBoolean("sendFindNames", true);
		fontSize = sharedPref.getInt("fontSize", 13);
		bright = sharedPref.getFloat("bright",0.5f);
		sensitivity = sharedPref.getInt("sensor", 210);
		createMovieBtn = sharedPref.getBoolean("createMovieBtn", false);
		blackMode = sharedPref.getBoolean("blackMode", false);
		newFootDlg = sharedPref.getBoolean("newFoot", true);
		directMove = sharedPref.getBoolean("directMove", true);
		if (D)
			Log.d(TAG, "sharedpref 생성자의 최초 callSavedTabs:" + callSavedTabs);
	}

	private void savePreferences(String key, boolean value) {
		Editor editor = sharedPref.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	public SharedPreferences getSharedPref() {
		return sharedPref;
	}

	public void setSharedPref(SharedPreferences sharedPref) {
		this.sharedPref = sharedPref;
	}

	public boolean isCallSavedTabs() {
		return callSavedTabs;
	}

	public void setCallSavedTabs(boolean callSavedTabs) {
		this.callSavedTabs = callSavedTabs;
		Editor editor = sharedPref.edit();
		editor.putBoolean("callSavedTabs", callSavedTabs);
		editor.commit();
	}

	public boolean isSendFindNames() {
		return sendFindNames;
	}

	public boolean isCreateMovieBtn() {
//		createMovieBtn = sharedPref.getBoolean("createMovieBtn", false);
		return createMovieBtn;
	}

	public void setSendFindNames(boolean sendFindNames) {
		this.sendFindNames = sendFindNames;
		Editor editor = sharedPref.edit();
		editor.putBoolean("sendFindNames", sendFindNames);
		editor.commit();
	}

	public int getFontSize() {
		return fontSize;
	}

	public void setFontSize(int fontSize) {
		this.fontSize = fontSize;
		Editor editor = sharedPref.edit();
		editor.putInt("fontSize", fontSize);
		editor.commit();
	}

	public void removeAll() {

		Editor editor = sharedPref.edit();
		editor.remove("callSavedNames");
		editor.commit();
	}

	public float getBright() {
		return bright;
	}

	public void setBright(float bright) { 
		this.bright = bright;
		Editor editor = sharedPref.edit();
		editor.putFloat("bright", bright);
		editor.commit();
	}

	public void setCreateMovieBtn(boolean createMovieButton) {
		this.createMovieBtn  =  createMovieButton;
		Editor editor = sharedPref.edit();
		editor.putBoolean ("createMovieBtn", createMovieButton);
		editor.commit();
		
	}

	public int getSensitivity() {
		return sensitivity;
	}

	public void setSensitivity(int sensitivity) {
		this.sensitivity = sensitivity;
		Editor editor = sharedPref.edit();
		editor.putInt("sensor", sensitivity);
		editor.commit();
	}

	public boolean isBlackMode() {
		return blackMode;
	}

	public void setBlackMode(boolean blackMode) {
		this.blackMode = blackMode;
		Editor editor = sharedPref.edit();
		editor.putBoolean ("blackMode",blackMode);
		editor.commit();
	}

	public boolean isNewFootDlg() {
		return newFootDlg;
	}

	public void setNewFootDlg(boolean newFootDlg) {
		this.newFootDlg = newFootDlg;
		Editor editor = sharedPref.edit();
		editor.putBoolean ("newFoot", newFootDlg);
		editor.commit();
	}

	public boolean isDirectMove() {
		return directMove;
	}

	public void setDirectMove(boolean directMove) {
		this.directMove = directMove; 
		Editor editor = sharedPref.edit();
		editor.putBoolean ("directMove",directMove);
		editor.commit();
	}
}
