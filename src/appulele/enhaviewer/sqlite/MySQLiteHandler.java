package appulele.enhaviewer.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import appulele.enhaviewer.URLs;

public class MySQLiteHandler {
	String TAG = "MySQLiteHandler";
	MySQLiteOpenHelper helper;
	SQLiteDatabase db;

	// 초기화 작업
	public MySQLiteHandler(Context context) {
		helper = new MySQLiteOpenHelper(context, "FindNames.sqlite", null, 1);

	}

	// open
	public static MySQLiteHandler open(Context context) {
		return new MySQLiteHandler(context);
	}

	// close
	public void close() {
		db.close();
	}

	// //저장
	// public void insert(String id, String parentName , String myName) {
	// db = helper.getWritableDatabase();
	// ContentValues values = new ContentValues();
	// values.put("id", id);
	// values.put("parentName", parentName);
	// values.put("myName",myName);
	// db.insert("person", null, values);
	// }//end insert

	// 저장
	public void insert(String url, String myName) {
		db = helper.getWritableDatabase();
		ContentValues values = new ContentValues();
		if (url == null)
			url = URLs.URLEnhaWiki+myName;
		values.put("url", url);
		values.put("myname", myName);
		db.insert("person", null, values);
		Log.d(TAG, "insert it");
	}// end insert

	// //수정
	// public void update(String id, int age) {
	// db = helper.getWritableDatabase();
	// ContentValues values = new ContentValues();
	// values.put("age", age);
	// db.update("person", values, "name=?", new String[]{name});
	// }//end update

	 //삭제
	 public void delete(String name) {
	 db = helper.getWritableDatabase();
	 db.delete("person", "myname=?", new String[]{name});
	 }//end delete

	// 검색
	public Cursor select() {
		db = helper.getReadableDatabase();
		Cursor c = db.query("person", null, null, null, null, null, null);
		Log.d(TAG,"select:"+c.getColumnName(0)+c.getColumnName(1)+c.getColumnName(2));
		return c;
	}// end select

	public void removeAll() {
		Log.i(TAG, "onDelete >>>>>>>>>>>>>>>.....");
		db = helper.getWritableDatabase( );
		String strSQL = "DROP TABLE IF EXISTS person;";
		db.execSQL(strSQL);
		strSQL = "CREATE TABLE IF NOT EXISTS person ( " + " _id integer primary key autoincrement , " + " url text , " + " myname text )";
 		db.execSQL(strSQL); 
 	
	}
}// end class