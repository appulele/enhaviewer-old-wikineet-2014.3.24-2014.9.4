package appulele.enhaviewer.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {
	// 데이터베이스 생성
	private static final String TAG = "MySQLiteOpenHelper";

	public MySQLiteOpenHelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	// 테이블 생성
	public void onCreate(SQLiteDatabase db) {
		Log.i(TAG, "onCreate >>>>>>>>>>>>>>>.....");

//		String sql = "create table person ( " + " _id integer primary key autoincrement , " + " parentname text , " + " myname text )";// +
		String strSQL = "CREATE TABLE IF NOT EXISTS person ( " + " _id integer primary key autoincrement , " + "url text , " + " myname text )";
 		db.execSQL(strSQL);
	}// end onCreate
		// 테이블 삭제

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i(TAG, "onUpgrade >>>>>>>>>>>>>>>.....");

		String sql = "drop table if exists person";
		db.execSQL(sql);

		onCreate(db);
	}// end onUpgrade
 
}// end class